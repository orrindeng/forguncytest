var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * ForguncyMenuCellType
 */
var Forguncy;
(function (Forguncy) {
    var ForguncyMenuCellTypeStyleTemplateHelper = /** @class */ (function () {
        function ForguncyMenuCellTypeStyleTemplateHelper() {
            this.CellTypeString = "Forguncy.CustomMenu.ForguncyMenuCellType";
            this.TemplateNameParts = [
                "LEVEL0",
                "LEVEL1",
                "LEVEL2",
                "LEVEL3",
            ];
        }
        ForguncyMenuCellTypeStyleTemplateHelper.prototype.ApplyTemplateStyle = function (styleTemplate, container) {
            if (!styleTemplate || !container) {
                return;
            }
            this.TemplateKey = styleTemplate.Key;
            var templateDomParts = this.MapPartsNameToDom(container);
            for (var i = 0; i < this.TemplateNameParts.length; i++) {
                var partName = this.TemplateNameParts[i];
                var className = Forguncy.CellTypeStyleTemplateUtils
                    .FormatTemplateStyleClassName(this.CellTypeString, this.TemplateKey, partName);
                var partDom = templateDomParts[partName];
                partDom.addClass(className);
            }
            this.OnTemplateStyleApplied(styleTemplate, container);
            this.Clear();
        };
        ForguncyMenuCellTypeStyleTemplateHelper.prototype.Clear = function () {
            this.Container = null;
            this.TemplateKey = null;
        };
        ForguncyMenuCellTypeStyleTemplateHelper.prototype.MapPartsNameToDom = function (container) {
            this.Container = container;
            return {
                LEVEL0: container.find("ul[level=0]>li>a"),
                LEVEL1: container.find("ul[level=1]>li>a"),
                LEVEL2: container.find("ul[level=2]>li>a"),
                LEVEL3: container.find("ul[level=3]>li>a"),
            };
        };
        ForguncyMenuCellTypeStyleTemplateHelper.prototype.OnTemplateStyleApplied = function (styleTemplate, container) {
        };
        return ForguncyMenuCellTypeStyleTemplateHelper;
    }());
    Forguncy.ForguncyMenuCellTypeStyleTemplateHelper = ForguncyMenuCellTypeStyleTemplateHelper;
    var ForguncyMenuCellType = /** @class */ (function (_super) {
        __extends(ForguncyMenuCellType, _super);
        function ForguncyMenuCellType() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.menuItemMaxHeight = 40;
            _this.menuItemHeightGap = 6;
            _this.menuItemMaxFontSize = 20;
            _this.menuItemFontSizeGap = 3;
            _this.nofiticationPaddingLeftRight = 7;
            _this.nofiticationMarginRight = 3;
            _this.spanTextMarginLeftRight = 10;
            _this.horizontalHyperlinkPaddingLeft = 8;
            _this.arrowWidthHeight = 16;
            _this.arrowMarginRight = 16;
            _this.defaultItemATag = null;
            _this.menuContainerName = "menuContainer";
            return _this;
        }
        Object.defineProperty(ForguncyMenuCellType.prototype, "orientation", {
            get: function () {
                var cellType = this.CellElement.CellType;
                return cellType.Orientation;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ForguncyMenuCellType.prototype, "canVerticallyStretch", {
            get: function () {
                var pageStretchInfo;
                var formularCalcContext = this.getFormulaCalcContext();
                if (formularCalcContext && Forguncy['PageManager'] && Forguncy['PageManager'].getPageStretchInfo) {
                    pageStretchInfo = Forguncy['PageManager'].getPageStretchInfo(formularCalcContext.PageID);
                }
                if (!pageStretchInfo) {
                    return false;
                }
                var currentStretchMode = pageStretchInfo.StretchMode;
                return currentStretchMode === 2 /* OnlyVertical */
                    || currentStretchMode === 3 /* BothDirection */
                    || currentStretchMode === 4 /* KeepRatioFillWidth */
                    || currentStretchMode === 5 /* KeepRatioFillHeight */;
            },
            enumerable: true,
            configurable: true
        });
        ForguncyMenuCellType.prototype.initMenuItemStyle = function (cellTypeMetaData, container) {
            if (cellTypeMetaData.Orientation === 0 /* Horizontal */) {
                //In default, menu items will share the width;
                container = this.setFirstItemLevelStyle(cellTypeMetaData, container);
                //If the level width has been set, used the set width;
                if (cellTypeMetaData.MenuLevelsStyle && cellTypeMetaData.MenuLevelsStyle[0] && cellTypeMetaData.MenuLevelsStyle[0].Width) {
                    container.find("> ul > li").css("width", cellTypeMetaData.MenuLevelsStyle[0].Width + "px");
                }
                //all li
                container.find("ul li").css("position", "relative");
                //all sub ul
                container.find("ul li ul").css("position", "absolute");
                //sub levels
                container.find("li li ul").css("position", "absolute");
                container.find("li li ul").css("top", "0");
                container.find("ul li ul").css("z-index", "10000000");
            }
            //When click the arrow of menu, the sub-menus all will be selected;
            //so disable user-select.
            container.find("ul").css("user-select", "none");
            //clear float arrow
            //20171010 edit by edward
            container.find("li a").css("overflow", "hidden");
            //default close all items or open all items
            if (cellTypeMetaData.DefaultExpandStyle === 0 /* OpenAll */ && cellTypeMetaData.Orientation === 1 /* Vertical */) {
                container.find("> ul i").addClass("down" /* Down */);
            }
            else {
                container.find("> ul ul").css("display", "none");
            }
        };
        ForguncyMenuCellType.prototype.initMenuItems = function (itemsInfo, levelsStyle, levelIndex, aTag) {
            if (itemsInfo != null && itemsInfo.length > 0) {
                var itemsLength = itemsInfo.length;
                var ul = $("<ul level=" + levelIndex + "></ul>");
                var hasChildren = false;
                for (var i = 0; i < itemsLength; i++) {
                    var itemInfo = itemsInfo[i];
                    //check authority
                    if (this.checkAuthority(itemInfo) === false) {
                        continue;
                    }
                    hasChildren = true;
                    var li = $("<li></li>");
                    var subMenusDom = this.initMenuItems(itemInfo.SubItems, levelsStyle, levelIndex + 1, aTag + ";index=" + i + ";level=" + (levelIndex + 1));
                    //append hyperlink
                    var a = this.createHyperlinkHtml(itemInfo, levelIndex, levelsStyle[levelIndex], aTag + ";index=" + i + "", subMenusDom != null);
                    li.append(a);
                    //append subItems
                    if (subMenusDom != null) {
                        li.append(subMenusDom);
                        var menuCellType = this.CellElement.CellType;
                        if (menuCellType.Orientation === 0 /* Horizontal */) {
                            //add hover event
                            if (levelIndex + 1 < levelsStyle.length) {
                                var width = levelsStyle[levelIndex + 1].Width;
                            }
                            this.showDropDownWhenHoverOnHorizontalLayout(li, width);
                        }
                    }
                    //append li
                    ul.append(li);
                }
                if (hasChildren === false) {
                    return null;
                }
                //set menu level style
                this.setMenuLevelStyle(ul, levelsStyle[levelIndex], levelIndex);
                return ul;
            }
            return null;
        };
        ForguncyMenuCellType.prototype.showDropDownWhenHoverOnHorizontalLayout = function (li, width) {
            var _this = this;
            li.hover(function () {
                var hyperlink = li.find("> a");
                var isExpand = $(hyperlink).attr("isExpand");
                //if the menu clicked has closed...
                if (isExpand === "false") {
                    //add active attribute for clicked element...
                    $(hyperlink).find("i").addClass("down" /* Down */);
                    //open menu items
                    $(hyperlink).next().css("display", "block");
                    var current_liWidth = $(hyperlink).parent().outerWidth(true);
                    var dropdown_ul = $(hyperlink).next();
                    var dropdown_ulLevel = parseInt(dropdown_ul.attr("level"));
                    //1.优先指定的下拉菜单的宽度；
                    //2.如果没有指定，对于二级下拉菜单的宽度，如果其宽度小于父级的宽度，优先使用父级的宽度，否则根据内容自适应；
                    //3.如果没有指定，对于二级以上下拉菜单的宽度，全部根据内容自适应；
                    var dropdownWidth = _this.getDropDown_ulMaxWidth(dropdown_ul);
                    if (width) {
                        dropdownWidth = width;
                    }
                    else if (dropdown_ulLevel === 1 && dropdownWidth < current_liWidth) {
                        dropdownWidth = current_liWidth;
                    }
                    dropdown_ul.css("width", dropdownWidth + "px");
                    if (dropdown_ulLevel > 1) {
                        dropdown_ul.css("left", current_liWidth + "px");
                    }
                    $(hyperlink).attr("isExpand", true);
                }
            }, function () {
                var hyperlink = li.find("> a");
                var isExpand = $(hyperlink).attr("isExpand");
                //if the menu clicked has opened...
                if (isExpand === "true") {
                    //close menu items
                    $(hyperlink).next().css("display", "none");
                    //remove down for arrow
                    $(hyperlink).find("i").removeClass("down");
                    $(hyperlink).attr("isExpand", false);
                }
            });
        };
        ForguncyMenuCellType.prototype.getDropDown_ulMaxWidth = function (dropdown_ul) {
            var allSubHyperlinks = dropdown_ul.find("> li > a");
            if (allSubHyperlinks.length > 0) {
                var iconMaxWidth = 0;
                var arroMaxwWidth = 0;
                var notifyMaxWidth = 0;
                var textMaxWidth = 0;
                for (var i = 0; i < allSubHyperlinks.length; i++) {
                    var subHyperlink = $(allSubHyperlinks[i]);
                    //find icon
                    if (subHyperlink.find("*[elemType='icon']").length > 0) {
                        var iconWidth = subHyperlink.find("*[elemType='icon']").outerWidth(true);
                        if (iconWidth > iconMaxWidth) {
                            iconMaxWidth = iconWidth;
                        }
                    }
                    //find the arrow;
                    if (subHyperlink.find("i[elemType='arrow']").length > 0) {
                        var arrowWidth = subHyperlink.find("i[elemType='arrow']").outerWidth(true);
                        if (arrowWidth > arroMaxwWidth) {
                            arroMaxwWidth = arrowWidth;
                        }
                    }
                    //find the notify icon width
                    if (subHyperlink.find("em[elemType='notification']").length > 0) {
                        var notifyWidth = subHyperlink.find("em[elemType='notification']").outerWidth(true);
                        if (notifyWidth > notifyMaxWidth) {
                            notifyMaxWidth = notifyWidth;
                        }
                    }
                    //find the text;
                    if (subHyperlink.find("span[elemType='text']").length > 0) {
                        //Floating-point error
                        var textWidth = subHyperlink.find("span[elemType='text']").outerWidth(true) + 1;
                        if (textWidth > textMaxWidth) {
                            textMaxWidth = textWidth;
                        }
                    }
                }
                return this.horizontalHyperlinkPaddingLeft + iconMaxWidth + arroMaxwWidth + notifyMaxWidth + textMaxWidth;
            }
        };
        ForguncyMenuCellType.prototype.setMenuLevelStyle = function (ul, style, levelIndex) {
            //call base
            _super.prototype.setMenuLevelStyle.call(this, ul, style, levelIndex);
            if (style) {
                if (style.Bold) {
                    ul.find("> li > a").css("font-weight", "bold");
                }
                if (style.Height != null && style.Height > 0) {
                    //Fix bug 23168:menu of level2 will disappear when try to hover it
                    var lineHeight = style.Height;
                    if (levelIndex == 0 && lineHeight > this.CellElement.Height) {
                        lineHeight = this.CellElement.Height;
                    }
                    ul.css("line-height", lineHeight + "px"); // 让里面的a居中
                }
                if (style.HoverForeColor) {
                    var cssSelector = "#" + this.ID + " ul[level='" + levelIndex + "'] > li > a:hover";
                    var pageName = this.getPageName();
                    Forguncy.MenuStyleUtils.InsertForeColorRule(pageName, cssSelector, style.HoverForeColor);
                }
                if (style.HoverBackColor) {
                    var cssSelector = "#" + this.ID + " ul[level='" + levelIndex + "'] > li > a:hover";
                    var pageName = this.getPageName();
                    Forguncy.MenuStyleUtils.InsertBackColorRule(pageName, cssSelector, style.HoverBackColor);
                }
            }
        };
        ForguncyMenuCellType.prototype.createHyperlinkHtml = function (itemInfo, levelIndex, style, aTag, hasArrow) {
            var _this = this;
            var isExpand = itemInfo.Expand;
            var cellTypeMetaData = this.CellElement.CellType;
            if (isExpand === undefined) {
                if (cellTypeMetaData.Orientation === 1 /* Vertical */) {
                    //update menu state for vertical menu item only
                    if (cellTypeMetaData.DefaultExpandStyle === 0 /* OpenAll */) {
                        isExpand = true;
                    }
                    else {
                        isExpand = false;
                    }
                }
            }
            if (cellTypeMetaData.Orientation === 0 /* Horizontal */) {
                isExpand = false;
            }
            this.updateMenuState(levelIndex, aTag, isExpand, true);
            itemInfo.aTag = aTag;
            var a = $("<a isExpand=" + isExpand + " aTag='" + aTag + "'></a>");
            if (itemInfo.IsDefaultItem) {
                this.defaultItemATag = aTag;
            }
            var itemStyle = {};
            itemStyle.itemHeight = this.menuItemMaxHeight - levelIndex * this.menuItemHeightGap;
            itemStyle.itemFontSize = this.menuItemMaxFontSize - levelIndex * this.menuItemFontSizeGap;
            _super.prototype.setHyperLinkStyle.call(this, itemStyle, style, levelIndex);
            //append icon
            if (itemInfo.IconPath && itemInfo.IconPath != "") {
                if (itemInfo.IsOldMenuPath) {
                    itemInfo.IsBuiltInIconPath = false;
                }
                a.append(this.createIconHtml(itemInfo.IconPath, itemInfo.IconColor, itemStyle.itemHeight, itemStyle.iconWidth, itemStyle.iconHeight, itemInfo.IsBuiltInIconPath, itemInfo.IsOldMenuPath));
            }
            //append text
            var text = itemInfo.Text;
            if (text === undefined || text === null) {
                text = "";
            }
            a.append($("<span elemType='text' style='margin-left:" + this.spanTextMarginLeftRight + "px; margin-right:" + this.spanTextMarginLeftRight + "px'>" + text + "</span>"));
            //append arrow
            if (hasArrow) {
                a.append(this.createArrowHtml(itemStyle.itemHeight, levelIndex));
            }
            if (itemInfo.Notification) {
                var span = $("<em elemType='notification'></em>");
                span.css("background-color", "red");
                span.css("color", "white");
                span.css("padding", "3px " + this.nofiticationPaddingLeftRight + "px");
                span.css("border-radius", 10);
                span.css("font-size", 12);
                span.css("font-weight", 700);
                span.css("line-height", 1);
                span.css("text-align", "center");
                span.css("font-style", "normal");
                span.css("float", "right");
                span.css("margin-top", (itemStyle.itemHeight - this.arrowWidthHeight) / 2);
                span.css("margin-right", this.nofiticationMarginRight);
                span.css("display", "none");
                this.notifyNodes.push({ "dom": span, "formula": itemInfo.Notification });
                a.append(span);
            }
            //add css style
            a.css("height", itemStyle.itemHeight + "px");
            a.css("line-height", itemStyle.itemHeight + "px");
            a.css("font-size", itemStyle.itemFontSize + "px");
            a.css("display", "block");
            a.css("cursor", "pointer");
            a.css("text-decoration", "none");
            //add click event
            if (cellTypeMetaData.Orientation === 0 /* Horizontal */) {
                a.css("padding-left", this.horizontalHyperlinkPaddingLeft + "px");
            }
            else {
                a.css("padding-left", (levelIndex + 1) * 15 + "px");
            }
            a.click(function (e) {
                var target = a;
                var aTag = target.attr("aTag");
                _this.setSelectItem(aTag);
                if (itemInfo.CommandList && itemInfo.CommandList.length > 0) {
                    _this.executeCommand(itemInfo.CommandList);
                }
                else {
                    _this.openCloseDropdownWhenClick(a, levelIndex);
                }
                return false;
            });
            return a;
        };
        ForguncyMenuCellType.prototype.createArrowHtml = function (itemHeight, levelIndex) {
            var _this = this;
            var i = $("<i elemType='arrow'></i>");
            var svg = $("\n<svg class=\"arrow\" width=\"64px\" height=\"64px\" viewBox=\"0 0 64 64\" version=\"1.1\" \nstyle=\"display:block; width: 100%; height: 100%;opacity:0.5\" \nxmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" >\n    <g class=\"arrowIcon\">\n        <polygon points=\"23.7562579 9 45.7140959 32.0000268 23.7562579 55.0000535 18.9046897 49.8969226 35.9750221 32.0000268 18.9046897 14.1031309\"></polygon>\n    </g>\n</svg>");
            i.addClass("fgc-plugin-menu-arrow" /* MenuArrow */);
            i.append(svg);
            //style
            i.css("width", this.arrowWidthHeight + "px");
            i.css("height", this.arrowWidthHeight + "px");
            i.css("margin-right", this.arrowMarginRight + "px");
            i.css("margin-top", (itemHeight - this.arrowWidthHeight) / 2 + "px");
            i.click(function () {
                var a = i.parent("a");
                _this.openCloseDropdownWhenClick(a, levelIndex);
                return false;
            });
            return i;
        };
        ForguncyMenuCellType.prototype.openCloseDropdownWhenClick = function (a, levelIndex) {
            var menuCellType = this.CellElement.CellType;
            if (menuCellType.Orientation === 0 /* Horizontal */) {
                return;
            }
            var isExpand = $(a).attr("isExpand");
            //if the menu clicked has opened...
            if (isExpand === "true") {
                //close menu items
                $(a).next().css("display", "none");
                //remove down for arrow
                $("i", a).removeClass("down");
                //update menu state
                this.updateMenuState(levelIndex, $(a).attr("aTag"), false);
                $(a).attr("isExpand", false);
            }
            else {
                //add active attribute for clicked element...
                $("i", a).addClass("down" /* Down */);
                //open menu items
                $(a).next().css("display", "block");
                //update menu state
                _super.prototype.updateMenuState.call(this, levelIndex, $(a).attr("aTag"), true);
                $(a).attr("isExpand", true);
            }
            this.updateSimpleBar();
        };
        ForguncyMenuCellType.prototype.onLoad = function () {
            //Horizontal menu will show dropdownObj, so overflow can't be 'hidden'.
            var innerContainer = $("#" + this.ID);
            var outerContainer = this.getOuterContainer();
            this.createSimpleBar(outerContainer);
            if (this.orientation === 0 /* Horizontal */) {
                /**
                 * 水平菜单 要允许下拉框伸出去
                 * Related to BugX 7486
                 */
                outerContainer.css("overflow", "");
                // handle stretch
                if (this.canVerticallyStretch) {
                    // 适应垂直拉伸
                    innerContainer.css('height', '100%')
                        .find('>ul').css('height', '100%')
                        .find('>li').css('height', '100%')
                        .find('>a').css({
                        'height': '100%',
                        'display': 'flex',
                        'align-items': 'center'
                    });
                    // 修改箭头和通知的icon只修改一级菜单的 因为之前适应垂直拉伸的逻辑也只应用于一级菜单
                    innerContainer.find('>ul>li>a i[elemtype="arrow"]').css('margin-top', '0');
                    innerContainer.find('>ul>li>a em[elemtype="notification"]').css('margin-top', '0');
                    // Fix Bug 28165: icon's location of menu is not right in page stretch mode.
                    innerContainer.find('>ul>li>a div[elemtype="icon"]').css('margin', '');
                    innerContainer.find('>ul>li>a span[elemtype="text"]').css('flex-grow', '1');
                }
            }
            else {
                /**
                 * 垂直菜单 不允许下拉框伸出去
                 * Related to BugX 7486
                 */
                outerContainer.css("overflow", "auto");
            }
            this.initNotifyNumber();
            var pageName = this.getPageName();
            if (Forguncy.MenuCellTypeBase.menuStorage == null || Forguncy.MenuCellTypeBase.menuStorage[pageName] == null || Forguncy.MenuCellTypeBase.menuStorage[pageName][this.ID] == null) {
                if (this.defaultItemATag) {
                    this.setDefaultItemStyle();
                }
                return;
            }
            var currentMenuStorage = Forguncy.MenuCellTypeBase.menuStorage[pageName][this.ID];
            if (currentMenuStorage["expand_tag"]) {
                for (var levelIndex = 0; levelIndex < currentMenuStorage["expand_tag"].length; levelIndex++) {
                    if (currentMenuStorage["expand_tag"][levelIndex] == null) {
                        return;
                    }
                    for (var tag in currentMenuStorage["expand_tag"][levelIndex]) {
                        var isExpand = currentMenuStorage["expand_tag"][levelIndex][tag];
                        this.setMenuStateForElement(levelIndex, tag, isExpand);
                    }
                }
            }
            if (currentMenuStorage["active_a_tag"]) {
                this.setSelectItem(currentMenuStorage["active_a_tag"]);
            }
            else if (this.defaultItemATag) {
                this.setDefaultItemStyle();
            }
            if (currentMenuStorage["scrollTop"]) {
                var self = this;
                var onMenuScrolled = function () {
                    $("#" + self.ID).parent(".simplebar-content").scrollTop(currentMenuStorage["scrollTop"]);
                    Forguncy.Page.unbind(Forguncy.PageEvents.PageDefaultDataLoaded, onMenuScrolled);
                };
                Forguncy.Page.bind(Forguncy.PageEvents.PageDefaultDataLoaded, onMenuScrolled);
            }
        };
        ForguncyMenuCellType.prototype.setBackColor = function (value) {
            if (value) {
                _super.prototype.setBackColor.call(this, value);
                return;
            }
            var cellTypeMetaData = this.CellElement.CellType;
            if (cellTypeMetaData.MenuLevelsStyle) {
                if (cellTypeMetaData.MenuLevelsStyle.length > 0) {
                    var firstLevelBackColor = cellTypeMetaData.MenuLevelsStyle[0].BackColor;
                    if (cellTypeMetaData.MenuLevelsStyle.length > 1) {
                        var secondLevelBackColor = cellTypeMetaData.MenuLevelsStyle[1].BackColor;
                    }
                }
            }
            var innerContainer = $("#" + this.ID);
            var outerContainer = this.getOuterContainer();
            if (this.orientation === 0 /* Horizontal */) {
                if (firstLevelBackColor) {
                    innerContainer.css("background", firstLevelBackColor);
                }
            }
            else {
                if (secondLevelBackColor) {
                    outerContainer.css("background", secondLevelBackColor);
                }
                else if (firstLevelBackColor) {
                    outerContainer.css("background", firstLevelBackColor);
                }
            }
        };
        ForguncyMenuCellType.prototype.setSelectItem = function (aTag) {
            $("#" + this.ID).find("a").removeClass("active");
            $("#" + this.ID).find("a[aTag='" + aTag + "']").addClass("active" /* Active */);
            _super.prototype.setSelectItem.call(this, aTag);
        };
        ForguncyMenuCellType.prototype.getStyleTemplateHelper = function () {
            return ForguncyMenuCellType.StyleTemplateHelper;
        };
        ForguncyMenuCellType.prototype.createIconHtml = function (iconPath, iconColor, itemHeight, iconWidth, iconHeight, isBuiltIn, isOldPath) {
            var imgHtml = _super.prototype.createIconHtml.call(this, iconPath, iconColor, itemHeight, iconWidth, iconHeight, isBuiltIn, isOldPath);
            imgHtml.css("margin-top", (itemHeight - iconHeight) / 2 + "px");
            imgHtml.css("float", "left");
            imgHtml.css("line-height", 0);
            return imgHtml;
        };
        ForguncyMenuCellType.prototype.setForeColorToLevel = function (menuCell, levelIndex, foreColor) {
            menuCell.find("ul[level=" + levelIndex + "]>li>a").css("color", foreColor);
            menuCell.find("ul[level=" + levelIndex + "]>li>a svg").attr("fill", foreColor);
        };
        /**
         * Override
         * Fix Bug 26622 [Menu]Menu's font color can't be changed by SetCellPropertyCommand.
         * @param styleInfo
         */
        ForguncyMenuCellType.prototype.setFontStyle = function (styleInfo) {
            if (!styleInfo) {
                return;
            }
            /**
             * Bug 28682: can't show CurrentUser's dropdown list if down cell's font size is large
             * 不设置menuContainer上的font-size
             */
            if (styleInfo.FontSize) {
                delete styleInfo.FontSize;
            }
            //Menu does not support underline property.
            if (styleInfo.Underline) {
                styleInfo.Underline = false;
            }
            _super.prototype.setFontStyle.call(this, styleInfo);
            var cellType = this.CellElement.CellType;
            var menuCell = $("#" + this.ID);
            if (styleInfo.Foreground) {
                for (var i = 0; i < cellType.MenuLevelsStyle.length; i++) {
                    var currentMenuLevelStyle = cellType.MenuLevelsStyle[i];
                    var usedForeColor = currentMenuLevelStyle.ForeColor
                        ? Forguncy.ConvertToCssColor(currentMenuLevelStyle.ForeColor)
                        : Forguncy.ConvertToCssColor(styleInfo.Foreground);
                    // 使用一次 然后置空 避免在设置条件格式的字体颜色时起干扰
                    currentMenuLevelStyle.ForeColor = null;
                    this.setForeColorToLevel(menuCell, i, usedForeColor);
                }
            }
            else {
                // clear 
                for (var i = 0; i < cellType.MenuLevelsStyle.length; i++) {
                    var currentMenuLevelStyle = cellType.MenuLevelsStyle[i];
                    currentMenuLevelStyle.ForeColor = null;
                }
            }
        };
        ForguncyMenuCellType.prototype.onWindowResized = function () {
            if (_super.prototype["onWindowResized" /* OnWindowResized */]) {
                _super.prototype["onWindowResized" /* OnWindowResized */].call(this);
                this.updateSimpleBar();
            }
        };
        ForguncyMenuCellType.prototype.getOuterContainer = function () {
            return $("#" + this.ID + "_div");
        };
        ForguncyMenuCellType.prototype.createSimpleBar = function (container) {
            if (this.orientation === 1 /* Vertical */) {
                this.simpleBar = new SimpleBar(container[0]);
                var self = this;
                $("#" + this.ID).parent(".simplebar-content").unbind("scroll");
                $("#" + this.ID).parent(".simplebar-content").bind("scroll", function () {
                    self.cacheScrollTop($(this).scrollTop());
                });
            }
        };
        ForguncyMenuCellType.prototype.updateSimpleBar = function () {
            this.simpleBar && this.simpleBar.recalculate();
        };
        ForguncyMenuCellType.prototype.cacheScrollTop = function (top) {
            this.ensureMenuStorage();
            var pageName = this.getPageName();
            Forguncy.MenuCellTypeBase.menuStorage[pageName][this.ID]["scrollTop"] = top;
        };
        ForguncyMenuCellType.StyleTemplateHelper = new ForguncyMenuCellTypeStyleTemplateHelper();
        return ForguncyMenuCellType;
    }(Forguncy.MenuCellTypeBase));
    Forguncy.ForguncyMenuCellType = ForguncyMenuCellType;
})(Forguncy || (Forguncy = {}));
Forguncy.Plugin.CellTypeHelper.registerCellType("Forguncy.CustomMenu.ForguncyMenuCellType, Forguncy.CustomMenu", Forguncy.ForguncyMenuCellType);
//# sourceMappingURL=ForguncyMenuCellType.js.map
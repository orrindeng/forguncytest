/**
 * MenuCellTypeBase
 *
 * */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Forguncy;
(function (Forguncy) {
    var MenuCellTypeBase = /** @class */ (function (_super) {
        __extends(MenuCellTypeBase, _super);
        function MenuCellTypeBase() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            //public menuStyleSheet: CSSStyleSheet = null;
            _this.notifyNodes = [];
            return _this;
        }
        MenuCellTypeBase.prototype.pageNavigated = function (arg1, arg2) {
            this.highlight(arg2.pageName);
        };
        MenuCellTypeBase.prototype.highlight = function (pageName) {
            var cellTypeMetaData = this.CellElement.CellType;
            var items = cellTypeMetaData.Items;
            var needHighlightItems = [];
            this.getNeedHighlightItems(items, pageName, needHighlightItems);
            if (!needHighlightItems || needHighlightItems.length === 0) {
                return;
            }
            if (this.isAnyItemSelected(needHighlightItems)) {
                return;
            }
            var needSelectItem = needHighlightItems[0];
            this.setSelectItem(needSelectItem.aTag);
        };
        MenuCellTypeBase.prototype.setSelectItem = function (aTag) {
            this.updateMenuActiveState(aTag);
        };
        MenuCellTypeBase.prototype.getNeedHighlightItems = function (items, pageName, result) {
            if (!items) {
                return;
            }
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                if (this.hasCommandNavigateToPage(item.CommandList, pageName)) {
                    result.push(item);
                }
                this.getNeedHighlightItems(item.SubItems, pageName, result);
            }
        };
        MenuCellTypeBase.prototype.hasCommandNavigateToPage = function (commandList, pageName) {
            if (commandList && commandList.length > 0) {
                for (var i = 0; i < commandList.length; i++) {
                    var command = commandList[i];
                    if (command && command["$type"] && command["$type"].indexOf("NavigateCommand") != -1 && command.PageName == pageName) {
                        return true;
                    }
                }
            }
            return false;
        };
        MenuCellTypeBase.prototype.isAnyItemSelected = function (items) {
            var menuStoragePageName = this.getPageName();
            var currentHighlightItem;
            if (MenuCellTypeBase.menuStorage[menuStoragePageName] && MenuCellTypeBase.menuStorage[menuStoragePageName][this.ID]) {
                currentHighlightItem = MenuCellTypeBase.menuStorage[menuStoragePageName][this.ID].active_a_tag;
            }
            if (!currentHighlightItem) {
                return false;
            }
            for (var i = 0; i < items.length; i++) {
                var aTag = items[i].aTag;
                if (aTag === currentHighlightItem) {
                    return true;
                }
            }
            return false;
        };
        MenuCellTypeBase.prototype.createContent = function () {
            var _this = this;
            Forguncy.Page.bind("pagedefaultdataloaded", this.pageNavigated.bind(this), "*");
            var element = this.CellElement;
            var cellTypeMetaData = element.CellType;
            // 根据模板调整样式
            this.adjustMenuStyleBasedOnTemplate(element);
            var container = $("<div id='" + this.ID + "' class=\"" + this.menuContainerName + "\"/>");
            for (var i in document.styleSheets) {
                if (document.styleSheets[i].href && document.styleSheets[i].href.indexOf("forguncyPluginCustomMenuStyle.css") !== -1) {
                    Forguncy.MenuStyleUtils.MenuStyleSheet = document.styleSheets[i];
                    break;
                }
            }
            this.removeNoPermissionItems(cellTypeMetaData);
            this.createMenuDom(cellTypeMetaData, container);
            this.initMenuItemStyle(cellTypeMetaData, container);
            this.onDependenceCellValueChanged(function () {
                _this.initNotifyNumber();
            });
            return container;
        };
        MenuCellTypeBase.prototype.initNotifyNumber = function () {
            var notices = this.notifyNodes;
            for (var i = 0; i < notices.length; i++) {
                var item = notices[i];
                var dom = item.dom;
                var formula = item.formula;
                var result = this.evaluateFormula(formula);
                if (result) {
                    dom.text(result);
                    dom.css("display", "block");
                }
                else {
                    dom.css("display", "none");
                }
            }
        };
        MenuCellTypeBase.prototype.destroy = function () {
            Forguncy.Page.unbind("pagedefaultdataloaded", this.pageNavigated, "*");
        };
        MenuCellTypeBase.prototype.removeNoPermissionItems = function (cellTypeMetaData) {
            var items = cellTypeMetaData.Items;
            for (var i = items.length - 1; i >= 0; i--) {
                var itemInfo = items[i];
                //check authority
                if (this.checkAuthority(itemInfo) === false) {
                    items.splice(i, 1);
                }
            }
        };
        MenuCellTypeBase.prototype.createMenuDom = function (cellTypeMetaData, container) {
            var items = cellTypeMetaData.Items;
            var levelsStyle = cellTypeMetaData.MenuLevelsStyle;
            this.notifyNodes = [];
            var ul = this.initMenuItems(items, levelsStyle, 0, "level=0");
            if (ul !== null) {
                container.append(ul);
            }
        };
        MenuCellTypeBase.prototype.initMenuItemStyle = function (cellTypeMetaData, container) {
        };
        MenuCellTypeBase.prototype.initMenuItems = function (itemsInfo, levelsStyle, levelIndex, aTag) {
            return null;
        };
        MenuCellTypeBase.prototype.setHyperLinkStyle = function (itemStyle, style, levelIndex) {
            if (style) {
                if (style.FontSize > 0) {
                    itemStyle.itemFontSize = style.FontSize;
                    itemStyle.itemHeight = style.FontSize * 2;
                }
                if (style.Height > 0) {
                    itemStyle.itemHeight = style.Height;
                }
            }
            itemStyle.itemHeight = Math.max(itemStyle.itemHeight, 20);
            //Fix bug 23168:menu of level2 will disappear when try to hover it
            if (levelIndex == 0 && itemStyle.itemHeight > this.CellElement.Height) {
                itemStyle.itemHeight = this.CellElement.Height;
            }
            itemStyle.iconWidth = itemStyle.itemHeight - 12;
            itemStyle.iconHeight = itemStyle.itemHeight;
            if (style) {
                if (style.IconWidth > 0) {
                    itemStyle.iconWidth = style.IconWidth;
                }
                if (style.IconHeight > 0) {
                    itemStyle.iconHeight = style.IconHeight;
                }
            }
            itemStyle.iconHeight = itemStyle.iconHeight > itemStyle.itemHeight ? itemStyle.itemHeight : itemStyle.iconHeight;
        };
        MenuCellTypeBase.prototype.checkAuthority = function (itemInfo) {
            var canVisitRoleList = itemInfo.CanVisitRoleList;
            if (canVisitRoleList != null) {
                if (canVisitRoleList.indexOf("FGC_Anonymous") !== -1) {
                    return true;
                }
                var userInfo = Forguncy.Page.getUserInfo();
                if (userInfo != null && userInfo.UserName != null && userInfo.UserName !== "") {
                    if (canVisitRoleList.indexOf("FGC_LoginUser") !== -1) {
                        return true;
                    }
                }
                var currentUserRoleList_1 = [];
                if (userInfo != null && userInfo.Role != null) {
                    currentUserRoleList_1 = userInfo.Role.split(",");
                }
                var intersectionArray = canVisitRoleList.filter(function (r) {
                    return currentUserRoleList_1.indexOf(r) !== -1;
                });
                if (intersectionArray.length !== 0) {
                    return true;
                }
            }
            return false;
        };
        MenuCellTypeBase.prototype.setMenuLevelStyle = function (ul, style, levelIndex) {
            var target = ul.find(">li>a");
            if (style) {
                if (style.BackColor) {
                    target.css("background", Forguncy.ConvertToCssColor(style.BackColor));
                    /**
                     * 在渐变色背景下 需要设置此属性 否则run起来的border下的背景会不正确
                     * Related Bug 26595: Display of Menu's border is not good in Runtime.
                     */
                    target.css("background-origin", "border-box");
                }
                if (style.ForeColor) {
                    target.css("color", Forguncy.ConvertToCssColor(style.ForeColor));
                    $(".arrow", target).attr("fill", Forguncy.ConvertToCssColor(style.ForeColor));
                }
                if (style.FontFamily) {
                    target.css("font-family", "'" + style.FontFamily + "'");
                }
                if (style.FontSize != null && style.FontSize > 0 && style.FontSize > style.Height) {
                    target.css("line-height", "2"); // 如果字号大于menu高度 则让menuItem也变高
                    target.css("font-size", style.FontSize + "px");
                }
                if (style.SelectedBackColor) {
                    var cssSelector = "#" + this.ID + " ul[level='" + levelIndex + "'] > li > a.active";
                    var pageName = this.getPageName();
                    Forguncy.MenuStyleUtils.InsertBackColorRule(pageName, cssSelector, Forguncy.ConvertToCssColor(style.SelectedBackColor));
                }
                if (style.SelectedForeColor) {
                    var cssSelector = "#" + this.ID + " ul[level='" + levelIndex + "'] > li > a.active";
                    var arrawCssSelector = "#" + this.ID + " ul[level='" + levelIndex + "'] > li > a.active g.arrowIcon";
                    var pageName = this.getPageName();
                    Forguncy.MenuStyleUtils.InsertForeColorRule(pageName, cssSelector, Forguncy.ConvertToCssColor(style.SelectedForeColor));
                    Forguncy.MenuStyleUtils.InsertArrowFillColorRule(pageName, arrawCssSelector, Forguncy.ConvertToCssColor(style.SelectedForeColor));
                }
            }
        };
        MenuCellTypeBase.prototype.setFirstItemLevelStyle = function (cellTypeMetaData, container) {
            container.find("> ul > li").css("display", "inline-block");
            container.find("> ul > li").css("vertical-align", "top");
            container.find("> ul > li").css("width", 100 / cellTypeMetaData.Items.length + "%");
            return container;
        };
        MenuCellTypeBase.prototype.updateMenuState = function (levelIndex, aTag, isExpand, isDefaultValue) {
            this.ensureMenuStorage();
            var pageName = this.getPageName();
            if (MenuCellTypeBase.menuStorage[pageName][this.ID]["expand_tag"] == null) {
                MenuCellTypeBase.menuStorage[pageName][this.ID]["expand_tag"] = [];
            }
            if (MenuCellTypeBase.menuStorage[pageName][this.ID]["expand_tag"][levelIndex] == null) {
                MenuCellTypeBase.menuStorage[pageName][this.ID]["expand_tag"][levelIndex] = [];
            }
            if (isDefaultValue) {
                if (MenuCellTypeBase.menuStorage[pageName][this.ID]["expand_tag"][levelIndex][aTag] == null) {
                    MenuCellTypeBase.menuStorage[pageName][this.ID]["expand_tag"][levelIndex][aTag] = isExpand;
                }
            }
            else {
                MenuCellTypeBase.menuStorage[pageName][this.ID]["expand_tag"][levelIndex][aTag] = isExpand;
            }
        };
        MenuCellTypeBase.prototype.updateMenuActiveState = function (aTag) {
            this.ensureMenuStorage();
            var pageName = this.getPageName();
            MenuCellTypeBase.menuStorage[pageName][this.ID]["active_a_tag"] = aTag;
        };
        MenuCellTypeBase.prototype.ensureMenuStorage = function () {
            if (!MenuCellTypeBase.menuStorage) {
                MenuCellTypeBase.menuStorage = {};
            }
            var pageName = this.getPageName();
            if (!MenuCellTypeBase.menuStorage[pageName]) {
                MenuCellTypeBase.menuStorage[pageName] = {};
            }
            if (!MenuCellTypeBase.menuStorage[pageName][this.ID]) {
                MenuCellTypeBase.menuStorage[pageName][this.ID] = {};
            }
        };
        MenuCellTypeBase.prototype.setDefaultItemStyle = function () {
            var a = $("#" + this.ID).find("a[aTag='" + this.defaultItemATag + "']");
            a.addClass("active" /* Active */);
            var parents = a.parents("ul");
            for (var i = 0; i < parents.length; i++) {
                var ul = $(parents[i]);
                if (ul.prev().attr("isExpand") == "false") {
                    ul.prev().attr("isExpand", true);
                    ul.prev().find("i").addClass("down" /* Down */);
                    ul.css("display", "block");
                }
            }
        };
        MenuCellTypeBase.prototype.setMenuStateForElement = function (levelIndex, aTag, isExpand) {
            var a = $("#" + this.ID).find("ul[level=" + levelIndex + "] > li > a[aTag='" + aTag + "']").first();
            $(a).attr("isExpand", isExpand);
            if (isExpand === true) {
                a.find("i").addClass("down" /* Down */);
                a.next().css("display", "block");
            }
            else {
                a.find("i").removeClass("down");
                a.next().css("display", "none");
            }
        };
        MenuCellTypeBase.prototype.adjustMenuStyleBasedOnTemplate = function (element) {
            var cellType = element.CellType;
            /**
             * Menu上的字体有3颜色:
             * A.Menu样式设置中的字体颜色（在有模板样式后就没有设置它的地方了，只在老版本中可设置）
             * B.Ribbon设置的单元格字体颜色
             * C.模板样式中的字体颜色
             * 优先级是：A > B > C
             * 策略：设置任何的C，会清空A和B
             * 在打开老版本的Demo，可能存在A和B，这时如果只设置B，应该不会有任何效果，因为A的优先级高，把B给覆盖了。如果想重新设置样式，则可先使用C清空B
             */
            if (this.CellElement.StyleInfo && this.CellElement.StyleInfo.Foreground) {
                for (var i = 0; i < cellType.MenuLevelsStyle.length; i++) {
                    var currentMenuLevelStyle = cellType.MenuLevelsStyle[i];
                    if (!currentMenuLevelStyle.ForeColor) {
                        currentMenuLevelStyle.ForeColor = this.CellElement.StyleInfo.Foreground;
                    }
                }
            }
            if (!element.StyleTemplate) {
                return;
            }
            if (!cellType.TemplateKey) {
                return null;
            }
            var styles = element.StyleTemplate.Styles;
            var cellParts = Object.keys(styles);
            for (var i = 0; i < cellParts.length; i++) {
                var partName = cellParts[i];
                var partStyle = styles[partName];
                this.mergeMenuItemStyle(partStyle, cellType.MenuLevelsStyle[i]);
            }
        };
        /**
         * 合并模板样式和单元格样式
         * 单元格样式的优先级 高于 模板样式
         */
        MenuCellTypeBase.prototype.mergeMenuItemStyle = function (templatePartStyle, originalStyle) {
            if (!originalStyle) {
                return;
            }
            for (var key in templatePartStyle) {
                var value = templatePartStyle[key];
                var style = value;
                if (key === "NormalStyle") {
                    if (style.Background && !originalStyle.BackColor) {
                        originalStyle.BackColor = Forguncy.ConvertToCssColor(style.Background);
                    }
                    if (style.FontColor && !originalStyle.ForeColor) {
                        originalStyle.ForeColor = Forguncy.ConvertToCssColor(style.FontColor);
                    }
                    if (style.FontSize && !originalStyle.FontSize) {
                        originalStyle.FontSize = style.FontSize;
                    }
                }
                else if (key === "HoverStyle") {
                    if (style.Background && !originalStyle.HoverBackColor) {
                        originalStyle.HoverBackColor = Forguncy.ConvertToCssColor(style.Background);
                    }
                    if (style.FontColor && !originalStyle.HoverForeColor) {
                        originalStyle.HoverForeColor = Forguncy.ConvertToCssColor(style.FontColor);
                    }
                }
                else if (key === "SelectedStyle") {
                    if (style.Background && !originalStyle.SelectedBackColor) {
                        originalStyle.SelectedBackColor = Forguncy.ConvertToCssColor(style.Background);
                    }
                    if (style.FontColor && !originalStyle.SelectedForeColor) {
                        originalStyle.SelectedForeColor = Forguncy.ConvertToCssColor(style.FontColor);
                    }
                }
            }
        };
        MenuCellTypeBase.prototype.getPageName = function () {
            return this.IsInMasterPage === true ? Forguncy.Page.getMasterPageName() : Forguncy.Page.getPageName();
        };
        MenuCellTypeBase.prototype.createIconHtml = function (iconPath, iconColor, itemHeight, iconWidth, iconHeight, isBuiltIn, isOldPath) {
            //20171010 edit by edward
            var imgHtml = $("<div elemType='icon'></div>");
            imgHtml.css("width", iconWidth + "px");
            imgHtml.css("height", iconHeight + "px");
            imgHtml.css("background-size", "contain");
            imgHtml.css("background-position", "center");
            imgHtml.css("background-repeat", "no-repeat");
            imgHtml.css("margin", "auto");
            imgHtml.css("line-height", "normal");
            if (isOldPath && !isBuiltIn) {
                imgHtml.css("background-image", "url(\"" + Forguncy.Helper.SpecialPath.getUploadFileFolderPathInDesigner() + "ForguncyCustomMenu/IconImages/" + encodeURIComponent(iconPath) + "\")");
            }
            else if (isBuiltIn && iconPath) {
                var src = Forguncy.Helper.SpecialPath.getBuiltInImageFolderPath() + iconPath;
                if (iconPath.length > 4 && iconPath.indexOf(".svg") == iconPath.length - 4) {
                    $.get(src, function (data) {
                        var svg = $(data.documentElement);
                        svg.css("width", "100%");
                        svg.css("height", "100%");
                        $("*", svg).attr("fill", Forguncy.ConvertToCssColor(iconColor));
                        imgHtml.append(svg);
                    });
                }
                else {
                    imgHtml.css("background-image", "url(\"" + src + "\")");
                }
            }
            else {
                imgHtml.css("background-image", "url(\"" + Forguncy.Helper.SpecialPath.getImageEditorUploadImageFolderPath() + encodeURIComponent(iconPath) + "\")");
            }
            return imgHtml;
        };
        MenuCellTypeBase.menuStorage = {};
        return MenuCellTypeBase;
    }(Forguncy.Plugin.CellTypeBase));
    Forguncy.MenuCellTypeBase = MenuCellTypeBase;
})(Forguncy || (Forguncy = {}));
//# sourceMappingURL=MenuCellTypeBase.js.map
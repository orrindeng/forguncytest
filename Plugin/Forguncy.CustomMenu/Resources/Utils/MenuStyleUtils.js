/**
 * Menu插件的样式工具方法
 */
var Forguncy;
(function (Forguncy) {
    var MenuStyleUtils = /** @class */ (function () {
        function MenuStyleUtils() {
        }
        MenuStyleUtils.InsertRule = function (pageName, cssSelector, ruleStr) {
            if (!MenuStyleUtils.MenuStyleSheet) {
                return false;
            }
            var ruleId = [pageName, cssSelector, ruleStr].join('*');
            if (MenuStyleUtils.UsedRuleCache[ruleId]) {
                return false;
            }
            MenuStyleUtils.MenuStyleSheet.insertRule(cssSelector + " " + ruleStr, 0);
            MenuStyleUtils.UsedRuleCache[ruleId] = true; // cache
            return true;
        };
        MenuStyleUtils.InsertArrowFillColorRule = function (pageName, cssSelector, fillColor, useImportant) {
            if (useImportant === void 0) { useImportant = true; }
            var ruleStr = "{fill: " + fillColor + " " + (useImportant ? '!important' : '') + ";}";
            return MenuStyleUtils.InsertRule(pageName, cssSelector, ruleStr);
        };
        MenuStyleUtils.InsertForeColorRule = function (pageName, cssSelector, foreColor, useImportant) {
            if (useImportant === void 0) { useImportant = true; }
            var ruleStr = "{color: " + foreColor + " " + (useImportant ? '!important' : '') + ";}";
            return MenuStyleUtils.InsertRule(pageName, cssSelector, ruleStr);
        };
        MenuStyleUtils.InsertBackColorRule = function (pageName, cssSelector, backgroundColor, useImportant) {
            if (useImportant === void 0) { useImportant = true; }
            /**
             * 在渐变色背景下 需要设置此属性 否则run起来的border下的背景会不正确
             * Related Bug 26595: Display of Menu's border is not good in Runtime.
             */
            var ruleStr = "{background: " + backgroundColor + " border-box " + (useImportant ? '!important' : '') + ";}";
            return MenuStyleUtils.InsertRule(pageName, cssSelector, ruleStr);
        };
        MenuStyleUtils.UsedRuleCache = {};
        return MenuStyleUtils;
    }());
    Forguncy.MenuStyleUtils = MenuStyleUtils;
})(Forguncy || (Forguncy = {}));
//# sourceMappingURL=MenuStyleUtils.js.map
[
    {
        "Key": "_RS_Dark_ThemeStyle1",
        "Category": "_RS_Dark",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Background 1 -20",
                    "Background": "Text 1 13"
                },
                "HoverStyle": {
                    "Background": "Text 1 26"
                },
                "FocusStyle": {
                    "Background": "Text 1 20"
                },
                "SelectedStyle": {
                    "Background": "Text 1 20"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Background 1 -20",
                    "Background": "Text 1"
                },
                "HoverStyle": {
                    "Background": "Text 1 13"
                },
                "FocusStyle": {
                    "FontColor": "Text 1 20",
                    "Background": "Background 1"
                },
                "SelectedStyle": {
                    "FontColor": "Text 1 20",
                    "Background": "Background 1"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Background 1 -20",
                    "Background": "Text 1"
                },
                "HoverStyle": {
                    "Background": "Text 1 13"
                },
                "FocusStyle": {
                    "FontColor": "Text 1 20",
                    "Background": "Background 1"
                },
                "SelectedStyle": {
                    "FontColor": "Text 1 20",
                    "Background": "Background 1"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Background 1 -20",
                    "Background": "Text 1"
                },
                "HoverStyle": {
                    "Background": "Text 1 13"
                },
                "FocusStyle": {
                    "FontColor": "Text 1 20",
                    "Background": "Background 1"
                },
                "SelectedStyle": {
                    "FontColor": "Text 1 20",
                    "Background": "Background 1"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Dark_ThemeStyle2",
        "Category": "_RS_Dark",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 1 12"
                },
                "HoverStyle": {
                    "Background": "Accent 1 27"
                },
                "FocusStyle": {
                    "Background": "Accent 1 20"
                },
                "SelectedStyle": {
                    "Background": "Accent 1 20"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 1"
                },
                "HoverStyle": {
                    "Background": "Accent 1 12"
                },
                "FocusStyle": {
                    "FontColor": "Accent 1",
                    "Background": "Background 1"
                },
                "SelectedStyle": {
                    "FontColor": "Accent 1",
                    "Background": "Background 1"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 1"
                },
                "HoverStyle": {
                    "Background": "Accent 1 12"
                },
                "FocusStyle": {
                    "FontColor": "Accent 1",
                    "Background": "Background 1"
                },
                "SelectedStyle": {
                    "FontColor": "Accent 1",
                    "Background": "Background 1"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 1"
                },
                "HoverStyle": {
                    "Background": "Accent 1 12"
                },
                "FocusStyle": {
                    "FontColor": "Accent 1",
                    "Background": "Background 1"
                },
                "SelectedStyle": {
                    "FontColor": "Accent 1",
                    "Background": "Background 1"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Dark_ThemeStyle3",
        "Category": "_RS_Dark",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 2 12"
                },
                "HoverStyle": {
                    "Background": "Accent 2 27"
                },
                "FocusStyle": {
                    "Background": "Accent 2 20"
                },
                "SelectedStyle": {
                    "Background": "Accent 2 20"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 2"
                },
                "HoverStyle": {
                    "Background": "Accent 2 12"
                },
                "FocusStyle": {
                    "FontColor": "Accent 2",
                    "Background": "Background 1"
                },
                "SelectedStyle": {
                    "FontColor": "Accent 2",
                    "Background": "Background 1"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 2"
                },
                "HoverStyle": {
                    "Background": "Accent 2 12"
                },
                "FocusStyle": {
                    "FontColor": "Accent 2",
                    "Background": "Background 1"
                },
                "SelectedStyle": {
                    "FontColor": "Accent 2",
                    "Background": "Background 1"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 2"
                },
                "HoverStyle": {
                    "Background": "Accent 2 12"
                },
                "FocusStyle": {
                    "FontColor": "Accent 2",
                    "Background": "Background 1"
                },
                "SelectedStyle": {
                    "FontColor": "Accent 2",
                    "Background": "Background 1"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Dark_ThemeStyle4",
        "Category": "_RS_Dark",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 3 12"
                },
                "HoverStyle": {
                    "Background": "Accent 3 27"
                },
                "FocusStyle": {
                    "Background": "Accent 3 20"
                },
                "SelectedStyle": {
                    "Background": "Accent 3 20"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 3"
                },
                "HoverStyle": {
                    "Background": "Accent 3 12"
                },
                "FocusStyle": {
                    "FontColor": "Accent 3",
                    "Background": "Background 1"
                },
                "SelectedStyle": {
                    "FontColor": "Accent 3",
                    "Background": "Background 1"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 3"
                },
                "HoverStyle": {
                    "Background": "Accent 3 12"
                },
                "FocusStyle": {
                    "FontColor": "Accent 3",
                    "Background": "Background 1"
                },
                "SelectedStyle": {
                    "FontColor": "Accent 3",
                    "Background": "Background 1"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 3"
                },
                "HoverStyle": {
                    "Background": "Accent 3 12"
                },
                "FocusStyle": {
                    "FontColor": "Accent 3",
                    "Background": "Background 1"
                },
                "SelectedStyle": {
                    "FontColor": "Accent 3",
                    "Background": "Background 1"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Dark_ThemeStyle5",
        "Category": "_RS_Dark",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 4 12"
                },
                "HoverStyle": {
                    "Background": "Accent 4 27"
                },
                "FocusStyle": {
                    "Background": "Accent 4 20"
                },
                "SelectedStyle": {
                    "Background": "Accent 4 20"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 4"
                },
                "HoverStyle": {
                    "Background": "Accent 4 12"
                },
                "FocusStyle": {
                    "FontColor": "Accent 4",
                    "Background": "Background 1"
                },
                "SelectedStyle": {
                    "FontColor": "Accent 4",
                    "Background": "Background 1"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 4"
                },
                "HoverStyle": {
                    "Background": "Accent 4 12"
                },
                "FocusStyle": {
                    "FontColor": "Accent 4",
                    "Background": "Background 1"
                },
                "SelectedStyle": {
                    "FontColor": "Accent 4",
                    "Background": "Background 1"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 4"
                },
                "HoverStyle": {
                    "Background": "Accent 4 12"
                },
                "FocusStyle": {
                    "FontColor": "Accent 4",
                    "Background": "Background 1"
                },
                "SelectedStyle": {
                    "FontColor": "Accent 4",
                    "Background": "Background 1"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Dark_ThemeStyle6",
        "Category": "_RS_Dark",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 5 12"
                },
                "HoverStyle": {
                    "Background": "Accent 5 27"
                },
                "FocusStyle": {
                    "Background": "Accent 5 20"
                },
                "SelectedStyle": {
                    "Background": "Accent 5 20"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 5"
                },
                "HoverStyle": {
                    "Background": "Accent 5 12"
                },
                "FocusStyle": {
                    "FontColor": "Accent 5",
                    "Background": "Background 1"
                },
                "SelectedStyle": {
                    "FontColor": "Accent 5",
                    "Background": "Background 1"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 5"
                },
                "HoverStyle": {
                    "Background": "Accent 5 12"
                },
                "FocusStyle": {
                    "FontColor": "Accent 5",
                    "Background": "Background 1"
                },
                "SelectedStyle": {
                    "FontColor": "Accent 5",
                    "Background": "Background 1"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 5"
                },
                "HoverStyle": {
                    "Background": "Accent 5 12"
                },
                "FocusStyle": {
                    "FontColor": "Accent 5",
                    "Background": "Background 1"
                },
                "SelectedStyle": {
                    "FontColor": "Accent 5",
                    "Background": "Background 1"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Dark_ThemeStyle7",
        "Category": "_RS_Dark",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 6 12"
                },
                "HoverStyle": {
                    "Background": "Accent 6 27"
                },
                "FocusStyle": {
                    "Background": "Accent 6 20"
                },
                "SelectedStyle": {
                    "Background": "Accent 6 20"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 6"
                },
                "HoverStyle": {
                    "Background": "Accent 6 12"
                },
                "FocusStyle": {
                    "FontColor": "Accent 6",
                    "Background": "Background 1"
                },
                "SelectedStyle": {
                    "FontColor": "Accent 6",
                    "Background": "Background 1"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 6"
                },
                "HoverStyle": {
                    "Background": "Accent 6 12"
                },
                "FocusStyle": {
                    "FontColor": "Accent 6",
                    "Background": "Background 1"
                },
                "SelectedStyle": {
                    "FontColor": "Accent 6",
                    "Background": "Background 1"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 6"
                },
                "HoverStyle": {
                    "Background": "Accent 6 12"
                },
                "FocusStyle": {
                    "FontColor": "Accent 6",
                    "Background": "Background 1"
                },
                "SelectedStyle": {
                    "FontColor": "Accent 6",
                    "Background": "Background 1"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Medium_ThemeStyle1",
        "Category": "_RS_Medium",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Text 1 47"
                },
                "HoverStyle": {
                    "Background": "Text 1 60"
                },
                "FocusStyle": {
                    "Background": "Text 1 54"
                },
                "SelectedStyle": {
                    "Background": "Text 1 54"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Text 1 20",
                    "Background": "Background 1",
                    "BorderBottomString": "1px solid Text_1_47"
                },
                "HoverStyle": {
                    "Background": "Text 1 93"
                },
                "FocusStyle": {
                    "Background": "Text 1 87"
                },
                "SelectedStyle": {
                    "Background": "Text 1 87"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Text 1 20",
                    "Background": "Background 1",
                    "BorderBottomString": "1px solid Text_1_47"
                },
                "HoverStyle": {
                    "Background": "Text 1 93"
                },
                "FocusStyle": {
                    "Background": "Text 1 87"
                },
                "SelectedStyle": {
                    "Background": "Text 1 87"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Text 1 20",
                    "Background": "Background 1",
                    "BorderBottomString": "1px solid Text_1_47"
                },
                "HoverStyle": {
                    "Background": "Text 1 93"
                },
                "FocusStyle": {
                    "Background": "Text 1 87"
                },
                "SelectedStyle": {
                    "Background": "Text 1 87"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Medium_ThemeStyle2",
        "Category": "_RS_Medium",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 1 27"
                },
                "HoverStyle": {
                    "Background": "Accent 1 52"
                },
                "FocusStyle": {
                    "Background": "Accent 1 42"
                },
                "SelectedStyle": {
                    "Background": "Accent 1 42"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Accent 1 27",
                    "Background": "Background 1",
                    "BorderBottomString": "1px solid Accent_1_27"
                },
                "HoverStyle": {
                    "Background": "Accent 1 89"
                },
                "FocusStyle": {
                    "Background": "Accent 1 85"
                },
                "SelectedStyle": {
                    "Background": "Accent 1 85"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Accent 1 27",
                    "Background": "Background 1",
                    "BorderBottomString": "1px solid Accent_1_27"
                },
                "HoverStyle": {
                    "Background": "Accent 1 89"
                },
                "FocusStyle": {
                    "Background": "Accent 1 85"
                },
                "SelectedStyle": {
                    "Background": "Accent 1 85"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Accent 1 27",
                    "Background": "Background 1",
                    "BorderBottomString": "1px solid Accent_1_27"
                },
                "HoverStyle": {
                    "Background": "Accent 1 89"
                },
                "FocusStyle": {
                    "Background": "Accent 1 85"
                },
                "SelectedStyle": {
                    "Background": "Accent 1 85"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Medium_ThemeStyle3",
        "Category": "_RS_Medium",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 2 27"
                },
                "HoverStyle": {
                    "Background": "Accent 2 52"
                },
                "FocusStyle": {
                    "Background": "Accent 2 42"
                },
                "SelectedStyle": {
                    "Background": "Accent 2 42"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Accent 2 27",
                    "Background": "Background 1",
                    "BorderBottomString": "1px solid Accent_2_27"
                },
                "HoverStyle": {
                    "Background": "Accent 2 89"
                },
                "FocusStyle": {
                    "Background": "Accent 2 85"
                },
                "SelectedStyle": {
                    "Background": "Accent 2 85"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Accent 2 27",
                    "Background": "Background 1",
                    "BorderBottomString": "1px solid Accent_2_27"
                },
                "HoverStyle": {
                    "Background": "Accent 2 89"
                },
                "FocusStyle": {
                    "Background": "Accent 2 85"
                },
                "SelectedStyle": {
                    "Background": "Accent 2 85"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Accent 2 27",
                    "Background": "Background 1",
                    "BorderBottomString": "1px solid Accent_2_27"
                },
                "HoverStyle": {
                    "Background": "Accent 2 89"
                },
                "FocusStyle": {
                    "Background": "Accent 2 85"
                },
                "SelectedStyle": {
                    "Background": "Accent 2 85"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Medium_ThemeStyle4",
        "Category": "_RS_Medium",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 3 27"
                },
                "HoverStyle": {
                    "Background": "Accent 3 52"
                },
                "FocusStyle": {
                    "Background": "Accent 3 42"
                },
                "SelectedStyle": {
                    "Background": "Accent 3 42"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Accent 3 27",
                    "Background": "Background 1",
                    "BorderBottomString": "1px solid Accent_3_27"
                },
                "HoverStyle": {
                    "Background": "Accent 3 89"
                },
                "FocusStyle": {
                    "Background": "Accent 3 85"
                },
                "SelectedStyle": {
                    "Background": "Accent 3 85"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Accent 3 27",
                    "Background": "Background 1",
                    "BorderBottomString": "1px solid Accent_3_27"
                },
                "HoverStyle": {
                    "Background": "Accent 3 89"
                },
                "FocusStyle": {
                    "Background": "Accent 3 85"
                },
                "SelectedStyle": {
                    "Background": "Accent 3 85"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Accent 3 27",
                    "Background": "Background 1",
                    "BorderBottomString": "1px solid Accent_3_27"
                },
                "HoverStyle": {
                    "Background": "Accent 3 89"
                },
                "FocusStyle": {
                    "Background": "Accent 3 85"
                },
                "SelectedStyle": {
                    "Background": "Accent 3 85"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Medium_ThemeStyle5",
        "Category": "_RS_Medium",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 4 27"
                },
                "HoverStyle": {
                    "Background": "Accent 4 52"
                },
                "FocusStyle": {
                    "Background": "Accent 4 42"
                },
                "SelectedStyle": {
                    "Background": "Accent 4 42"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Accent 4 27",
                    "Background": "Background 1",
                    "BorderBottomString": "1px solid Accent_4_27"
                },
                "HoverStyle": {
                    "Background": "Accent 4 89"
                },
                "FocusStyle": {
                    "Background": "Accent 4 85"
                },
                "SelectedStyle": {
                    "Background": "Accent 4 85"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Accent 4 27",
                    "Background": "Background 1",
                    "BorderBottomString": "1px solid Accent_4_27"
                },
                "HoverStyle": {
                    "Background": "Accent 4 89"
                },
                "FocusStyle": {
                    "Background": "Accent 4 85"
                },
                "SelectedStyle": {
                    "Background": "Accent 4 85"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Accent 4 27",
                    "Background": "Background 1",
                    "BorderBottomString": "1px solid Accent_4_27"
                },
                "HoverStyle": {
                    "Background": "Accent 4 89"
                },
                "FocusStyle": {
                    "Background": "Accent 4 85"
                },
                "SelectedStyle": {
                    "Background": "Accent 4 85"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Medium_ThemeStyle6",
        "Category": "_RS_Medium",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 5 27"
                },
                "HoverStyle": {
                    "Background": "Accent 5 52"
                },
                "FocusStyle": {
                    "Background": "Accent 5 42"
                },
                "SelectedStyle": {
                    "Background": "Accent 5 42"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Accent 5 27",
                    "Background": "Background 1",
                    "BorderBottomString": "1px solid Accent_5_27"
                },
                "HoverStyle": {
                    "Background": "Accent 5 89"
                },
                "FocusStyle": {
                    "Background": "Accent 5 85"
                },
                "SelectedStyle": {
                    "Background": "Accent 5 85"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Accent 5 27",
                    "Background": "Background 1",
                    "BorderBottomString": "1px solid Accent_5_27"
                },
                "HoverStyle": {
                    "Background": "Accent 5 89"
                },
                "FocusStyle": {
                    "Background": "Accent 5 85"
                },
                "SelectedStyle": {
                    "Background": "Accent 5 85"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Accent 5 27",
                    "Background": "Background 1",
                    "BorderBottomString": "1px solid Accent_5_27"
                },
                "HoverStyle": {
                    "Background": "Accent 5 89"
                },
                "FocusStyle": {
                    "Background": "Accent 5 85"
                },
                "SelectedStyle": {
                    "Background": "Accent 5 85"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Medium_ThemeStyle7",
        "Category": "_RS_Medium",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "Accent 6 27"
                },
                "HoverStyle": {
                    "Background": "Accent 6 52"
                },
                "FocusStyle": {
                    "Background": "Accent 6 42"
                },
                "SelectedStyle": {
                    "Background": "Accent 6 42"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Accent 6 27",
                    "Background": "Background 1",
                    "BorderBottomString": "1px solid Accent_6_27"
                },
                "HoverStyle": {
                    "Background": "Accent 6 89"
                },
                "FocusStyle": {
                    "Background": "Accent 6 85"
                },
                "SelectedStyle": {
                    "Background": "Accent 6 85"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Accent 6 27",
                    "Background": "Background 1",
                    "BorderBottomString": "1px solid Accent_6_27"
                },
                "HoverStyle": {
                    "Background": "Accent 6 89"
                },
                "FocusStyle": {
                    "Background": "Accent 6 85"
                },
                "SelectedStyle": {
                    "Background": "Accent 6 85"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Accent 6 27",
                    "Background": "Background 1",
                    "BorderBottomString": "1px solid Accent_6_27"
                },
                "HoverStyle": {
                    "Background": "Accent 6 89"
                },
                "FocusStyle": {
                    "Background": "Accent 6 85"
                },
                "SelectedStyle": {
                    "Background": "Accent 6 85"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Light_ThemeStyle1",
        "Category": "_RS_Light",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Text 1 20",
                    "Background": "Text 1 80"
                },
                "HoverStyle": {
                    "Background": "Text 1 74"
                },
                "FocusStyle": {
                    "Background": "Text 1 70"
                },
                "SelectedStyle": {
                    "Background": "Text 1 70"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Text 1 20",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Text 1 74"
                },
                "FocusStyle": {
                    "Background": "Text 1 70"
                },
                "SelectedStyle": {
                    "Background": "Text 1 70"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Text 1 20",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Text 1 74"
                },
                "FocusStyle": {
                    "Background": "Text 1 70"
                },
                "SelectedStyle": {
                    "Background": "Text 1 70"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Text 1 20",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Text 1 74"
                },
                "FocusStyle": {
                    "Background": "Text 1 70"
                },
                "SelectedStyle": {
                    "Background": "Text 1 70"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Light_ThemeStyle2",
        "Category": "_RS_Light",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Accent 1 14",
                    "Background": "Accent 1 75"
                },
                "HoverStyle": {
                    "Background": "Accent 1 72"
                },
                "FocusStyle": {
                    "Background": "Accent 1 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 1 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Accent 1 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 1 72"
                },
                "FocusStyle": {
                    "Background": "Accent 1 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 1 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Accent 1 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 1 72"
                },
                "FocusStyle": {
                    "Background": "Accent 1 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 1 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Accent 1 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 1 72"
                },
                "FocusStyle": {
                    "Background": "Accent 1 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 1 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Light_ThemeStyle3",
        "Category": "_RS_Light",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Accent 2 14",
                    "Background": "Accent 2 75"
                },
                "HoverStyle": {
                    "Background": "Accent 2 72"
                },
                "FocusStyle": {
                    "Background": "Accent 2 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 2 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Accent 2 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 2 72"
                },
                "FocusStyle": {
                    "Background": "Accent 2 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 2 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Accent 2 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 2 72"
                },
                "FocusStyle": {
                    "Background": "Accent 2 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 2 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Accent 2 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 2 72"
                },
                "FocusStyle": {
                    "Background": "Accent 2 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 2 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Light_ThemeStyle4",
        "Category": "_RS_Light",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Accent 3 14",
                    "Background": "Accent 3 75"
                },
                "HoverStyle": {
                    "Background": "Accent 3 72"
                },
                "FocusStyle": {
                    "Background": "Accent 3 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 3 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Accent 3 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 3 72"
                },
                "FocusStyle": {
                    "Background": "Accent 3 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 3 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Accent 3 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 3 72"
                },
                "FocusStyle": {
                    "Background": "Accent 3 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 3 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Accent 3 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 3 72"
                },
                "FocusStyle": {
                    "Background": "Accent 3 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 3 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Light_ThemeStyle5",
        "Category": "_RS_Light",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Accent 4 14",
                    "Background": "Accent 4 75"
                },
                "HoverStyle": {
                    "Background": "Accent 4 72"
                },
                "FocusStyle": {
                    "Background": "Accent 4 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 4 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Accent 4 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 4 72"
                },
                "FocusStyle": {
                    "Background": "Accent 4 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 4 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Accent 4 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 4 72"
                },
                "FocusStyle": {
                    "Background": "Accent 4 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 4 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Accent 4 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 4 72"
                },
                "FocusStyle": {
                    "Background": "Accent 4 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 4 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Light_ThemeStyle6",
        "Category": "_RS_Light",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Accent 5 14",
                    "Background": "Accent 5 75"
                },
                "HoverStyle": {
                    "Background": "Accent 5 72"
                },
                "FocusStyle": {
                    "Background": "Accent 5 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 5 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Accent 5 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 5 72"
                },
                "FocusStyle": {
                    "Background": "Accent 5 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 5 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Accent 5 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 5 72"
                },
                "FocusStyle": {
                    "Background": "Accent 5 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 5 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Accent 5 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 5 72"
                },
                "FocusStyle": {
                    "Background": "Accent 5 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 5 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Light_ThemeStyle7",
        "Category": "_RS_Light",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Accent 6 14",
                    "Background": "Accent 6 75"
                },
                "HoverStyle": {
                    "Background": "Accent 6 72"
                },
                "FocusStyle": {
                    "Background": "Accent 6 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 6 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Accent 6 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 6 72"
                },
                "FocusStyle": {
                    "Background": "Accent 6 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 6 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Accent 6 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 6 72"
                },
                "FocusStyle": {
                    "Background": "Accent 6 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 6 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Accent 6 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 6 72"
                },
                "FocusStyle": {
                    "Background": "Accent 6 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 6 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Gradient_ThemeStyle1",
        "Category": "_RS_Gradient",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "linear-gradient(90deg, Text_1_47 10%, Text_1_80 100%);"
                },
                "HoverStyle": {
                    "Background": "linear-gradient(90deg, Text_1_60 10%, Text_1_80 100%);"
                },
                "FocusStyle": {
                    "Background": "linear-gradient(90deg, Text_1_54 10%, Text_1_80 100%);"
                },
                "SelectedStyle": {
                    "Background": "linear-gradient(90deg, Text_1_54 10%, Text_1_80 100%);"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Text 1 20",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Text 1 74"
                },
                "FocusStyle": {
                    "Background": "Text 1 70"
                },
                "SelectedStyle": {
                    "Background": "Text 1 70"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Text 1 20",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Text 1 74"
                },
                "FocusStyle": {
                    "Background": "Text 1 70"
                },
                "SelectedStyle": {
                    "Background": "Text 1 70"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Text 1 20",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Text 1 74"
                },
                "FocusStyle": {
                    "Background": "Text 1 70"
                },
                "SelectedStyle": {
                    "Background": "Text 1 70"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Gradient_ThemeStyle2",
        "Category": "_RS_Gradient",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "linear-gradient(90deg, Accent_1_27 10%, Accent_1_80 100%);"
                },
                "HoverStyle": {
                    "Background": "linear-gradient(90deg, Accent_1_52 10%, Accent_1_80 100%);"
                },
                "FocusStyle": {
                    "Background": "linear-gradient(90deg, Accent_1_42 10%, Accent_1_80 100%);"
                },
                "SelectedStyle": {
                    "Background": "linear-gradient(90deg, Accent_1_42 10%, Accent_1_80 100%);"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Accent 1 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 1 72"
                },
                "FocusStyle": {
                    "Background": "Accent 1 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 1 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Accent 1 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 1 72"
                },
                "FocusStyle": {
                    "Background": "Accent 1 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 1 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Accent 1 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 1 72"
                },
                "FocusStyle": {
                    "Background": "Accent 1 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 1 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Gradient_ThemeStyle3",
        "Category": "_RS_Gradient",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "linear-gradient(90deg, Accent_2_27 10%, Accent_2_80 100%);"
                },
                "HoverStyle": {
                    "Background": "linear-gradient(90deg, Accent_2_52 10%, Accent_2_80 100%);"
                },
                "FocusStyle": {
                    "Background": "linear-gradient(90deg, Accent_2_42 10%, Accent_2_80 100%);"
                },
                "SelectedStyle": {
                    "Background": "linear-gradient(90deg, Accent_2_42 10%, Accent_2_80 100%);"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Accent 2 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 2 72"
                },
                "FocusStyle": {
                    "Background": "Accent 2 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 2 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Accent 2 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 2 72"
                },
                "FocusStyle": {
                    "Background": "Accent 2 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 2 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Accent 2 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 2 72"
                },
                "FocusStyle": {
                    "Background": "Accent 2 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 2 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Gradient_ThemeStyle4",
        "Category": "_RS_Gradient",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "linear-gradient(90deg, Accent_3_27 10%, Accent_3_80 100%);"
                },
                "HoverStyle": {
                    "Background": "linear-gradient(90deg, Accent_3_52 10%, Accent_3_80 100%);"
                },
                "FocusStyle": {
                    "Background": "linear-gradient(90deg, Accent_3_42 10%, Accent_3_80 100%);"
                },
                "SelectedStyle": {
                    "Background": "linear-gradient(90deg, Accent_3_42 10%, Accent_3_80 100%);"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Accent 3 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 3 72"
                },
                "FocusStyle": {
                    "Background": "Accent 3 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 3 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Accent 3 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 3 72"
                },
                "FocusStyle": {
                    "Background": "Accent 3 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 3 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Accent 3 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 3 72"
                },
                "FocusStyle": {
                    "Background": "Accent 3 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 3 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Gradient_ThemeStyle5",
        "Category": "_RS_Gradient",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "linear-gradient(90deg, Accent_4_27 10%, Accent_4_80 100%);"
                },
                "HoverStyle": {
                    "Background": "linear-gradient(90deg, Accent_4_52 10%, Accent_4_80 100%);"
                },
                "FocusStyle": {
                    "Background": "linear-gradient(90deg, Accent_4_42 10%, Accent_4_80 100%);"
                },
                "SelectedStyle": {
                    "Background": "linear-gradient(90deg, Accent_4_42 10%, Accent_4_80 100%);"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Accent 4 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 4 72"
                },
                "FocusStyle": {
                    "Background": "Accent 4 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 4 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Accent 4 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 4 72"
                },
                "FocusStyle": {
                    "Background": "Accent 4 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 4 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Accent 4 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 4 72"
                },
                "FocusStyle": {
                    "Background": "Accent 4 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 4 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Gradient_ThemeStyle6",
        "Category": "_RS_Gradient",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "linear-gradient(90deg, Accent_5_27 10%, Accent_5_80 100%);"
                },
                "HoverStyle": {
                    "Background": "linear-gradient(90deg, Accent_5_52 10%, Accent_5_80 100%);"
                },
                "FocusStyle": {
                    "Background": "linear-gradient(90deg, Accent_5_42 10%, Accent_5_80 100%);"
                },
                "SelectedStyle": {
                    "Background": "linear-gradient(90deg, Accent_5_42 10%, Accent_5_80 100%);"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Accent 5 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 5 72"
                },
                "FocusStyle": {
                    "Background": "Accent 5 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 5 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Accent 5 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 5 72"
                },
                "FocusStyle": {
                    "Background": "Accent 5 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 5 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Accent 5 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 5 72"
                },
                "FocusStyle": {
                    "Background": "Accent 5 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 5 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    },
    {
        "Key": "_RS_Gradient_ThemeStyle7",
        "Category": "_RS_Gradient",
        "Styles": {
            "LEVEL0": {
                "NormalStyle": {
                    "FontColor": "Background 1",
                    "Background": "linear-gradient(90deg, Accent_6_27 10%, Accent_6_80 100%);"
                },
                "HoverStyle": {
                    "Background": "linear-gradient(90deg, Accent_6_52 10%, Accent_6_80 100%);"
                },
                "FocusStyle": {
                    "Background": "linear-gradient(90deg, Accent_6_42 10%, Accent_6_80 100%);"
                },
                "SelectedStyle": {
                    "Background": "linear-gradient(90deg, Accent_6_42 10%, Accent_6_80 100%);"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL1": {
                "NormalStyle": {
                    "FontColor": "Accent 6 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 6 72"
                },
                "FocusStyle": {
                    "Background": "Accent 6 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 6 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL2": {
                "NormalStyle": {
                    "FontColor": "Accent 6 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 6 72"
                },
                "FocusStyle": {
                    "Background": "Accent 6 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 6 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            },
            "LEVEL3": {
                "NormalStyle": {
                    "FontColor": "Accent 6 14",
                    "Background": "Background 1"
                },
                "HoverStyle": {
                    "Background": "Accent 6 72"
                },
                "FocusStyle": {
                    "Background": "Accent 6 67"
                },
                "SelectedStyle": {
                    "Background": "Accent 6 67"
                },
                "DisableStyle": {},
                "Transition": "0.15s"
            }
        }
    }
];
//# sourceMappingURL=ForguncyMenu.js.map
/**
 * ForguncyMobileMenuCellType
 *
 **/
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Forguncy;
(function (Forguncy) {
    var ForguncyMobileMenuCellType = /** @class */ (function (_super) {
        __extends(ForguncyMobileMenuCellType, _super);
        function ForguncyMobileMenuCellType() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.menuContainerName = "mobileMenuContainer";
            return _this;
        }
        ForguncyMobileMenuCellType.prototype.createContent = function () {
            var container = Forguncy.MenuCellTypeBase.prototype.createContent.call(this);
            container.css("z-index", "9999");
            return container;
        };
        ForguncyMobileMenuCellType.prototype.initMenuItemStyle = function (cellTypeMetaData, container) {
            var rootLevelIndex = 0;
            container = this.setFirstItemLevelStyle(cellTypeMetaData, container);
            //all li
            $(container).find("ul li").css("position", "relative");
            var rootLevelHeight = cellTypeMetaData.MenuLevelsStyle[rootLevelIndex].Height;
            var childNodeWidth = document.documentElement.clientWidth / cellTypeMetaData.Items.length;
            $(container).find("ul li ul").css("min-width", childNodeWidth + "px");
            $(container).find("ul li ul").css("width", "auto");
            $(container).find("ul li ul").css("position", "absolute");
            $(container).find("ul li ul").css("bottom", rootLevelHeight + "px");
            $(container).find("ul li ul").css("z-index", "10000000");
            $(container).find("li a").css("overflow", "hidden");
            $(container).find("li a").css("text-overflow", "ellipsis");
            if ($(container).find("> ul > li").last().find("> ul").length > 0) {
                $(container).find("> ul > li > ul").last().css("right", "0px");
            }
            $(container).find("> ul ul").css("display", "none");
        };
        ForguncyMobileMenuCellType.prototype.initMenuItems = function (itemsInfo, levelsStyle, levelIndex) {
            if (itemsInfo != null && itemsInfo.length > 0) {
                var itemsLength = itemsInfo.length;
                var ul = $("<ul></ul>");
                var hasChildren = false;
                for (var i = 0; i < itemsLength; i++) {
                    var itemInfo = itemsInfo[i];
                    //check authority
                    if (this.checkAuthority(itemInfo) === false) {
                        continue;
                    }
                    hasChildren = true;
                    var li = $("<li></li>");
                    var subMenusDom = this.initMenuItems(itemInfo.SubItems, levelsStyle, levelIndex + 1);
                    //append hyperlink
                    var a = this.createHyperlinkHtml(itemInfo, levelIndex, levelsStyle[levelIndex], subMenusDom != null);
                    li.append(a);
                    //append subItems
                    if (subMenusDom != null) {
                        li.append(subMenusDom);
                    }
                    //append li
                    ul.append(li);
                }
                if (hasChildren === false) {
                    return null;
                }
                //set menu level style
                this.setMenuLevelStyle(ul, levelsStyle[levelIndex], levelIndex);
                if (levelIndex == 0) {
                    ul.css("line-height", "inherit");
                    ul.css("font-size", "inherit");
                }
                return ul;
            }
            return null;
        };
        // override
        ForguncyMobileMenuCellType.prototype.setMenuLevelStyle = function (ul, style, levelIndex) {
            _super.prototype.setMenuLevelStyle.call(this, ul, style, levelIndex);
            /**
             * Related to Bug 26648 The Display of phone menu list is not good.
             */
            var target = ul.find(">li");
            if (style) {
                if (style.BackColor) {
                    target.css("background", Forguncy.ConvertToCssColor(style.BackColor));
                }
            }
        };
        ForguncyMobileMenuCellType.prototype.createHyperlinkHtml = function (itemInfo, levelIndex, style, hasArrow) {
            var cellTypeMetaData = this.CellElement.CellType;
            var a = $("<a></a>");
            var itemStyle = {};
            Forguncy.MenuCellTypeBase.prototype.setHyperLinkStyle.call(this, itemStyle, style, levelIndex);
            //append text
            a.append($("<span>" + itemInfo.Text + "</span>"));
            //append arrow
            if (hasArrow === true) {
                a.append(this.createArrowHtml(itemStyle.itemHeight, levelIndex, cellTypeMetaData.Orientation));
            }
            if (itemInfo.Notification) {
                var span = $("<span></span>");
                span.css("background-color", "red");
                span.css("color", "white");
                span.css("font-size", 12);
                span.css("font-weight", 700);
                span.css("text-align", "center");
                span.css("font-family", Forguncy.LocaleFonts.default);
                span.css("font-style", "normal");
                span.css("display", "none");
                span.css("position", "absolute");
                span.css("right", 5);
                span.css("top", 5);
                span.css("height", "15px");
                span.css("width", "15px");
                span.css("border-radius", "10px");
                span.css("text-overflow", "ellipsis");
                span.css("line-height", "15px");
                span.css("overflow", "hidden");
                span.css("padding", "2px");
                this.notifyNodes.push({ "dom": span, "formula": itemInfo.Notification });
                a.append(span);
            }
            //add css style
            a.css("height", itemStyle.itemHeight + "px");
            a.css("line-height", itemStyle.itemHeight + "px");
            a.css("font-size", itemStyle.itemFontSize + "px");
            a.css("display", "block");
            a.css("cursor", "pointer");
            a.css("text-decoration", "none");
            var self = this;
            //bind parent node click event
            a.click(function (event) {
                event.stopPropagation();
                event.preventDefault();
                var aLink = a;
                if (aLink.next().length > 0) {
                    if (aLink.hasClass("open" /* Open */)) {
                        aLink.next().css("display", "none");
                        aLink.removeClass("open" /* Open */);
                    }
                    else {
                        $(".mobileMenuContainer ul li a").removeClass("open" /* Open */);
                        aLink.parent().parent().find("ul").css("display", "none");
                        aLink.next().css("display", "block");
                        aLink.addClass("open" /* Open */);
                    }
                }
                if (itemInfo.CommandList && itemInfo.CommandList.length > 0) {
                    self.executeCommand(itemInfo.CommandList);
                    $(".mobileMenuContainer ul li a").removeClass("open" /* Open */);
                    $(".mobileMenuContainer ul ul").css("display", "none");
                }
                return;
            });
            return a;
        };
        ForguncyMobileMenuCellType.prototype.createArrowHtml = function (itemHeight, levelIndex, orientation) {
            var i = $("<i></i>");
            i.addClass("mobilearrow" /* MobileArrow */);
            return i;
        };
        ForguncyMobileMenuCellType.prototype.onLoad = function () {
            this.initNotifyNumber();
            $("#" + this.ID).parent("div").width(0);
        };
        return ForguncyMobileMenuCellType;
    }(Forguncy.MenuCellTypeBase));
    Forguncy.ForguncyMobileMenuCellType = ForguncyMobileMenuCellType;
})(Forguncy || (Forguncy = {}));
Forguncy.Plugin.CellTypeHelper.registerCellType("Forguncy.CustomMenu.ForguncyMobileMenuCellType, Forguncy.CustomMenu", Forguncy.ForguncyMobileMenuCellType);
document.onclick = function () {
    $(".mobileMenuContainer ul li a").removeClass("open");
    $(".mobileMenuContainer ul ul").css("display", "none");
};
//# sourceMappingURL=ForguncyMobileMenuCellType.js.map
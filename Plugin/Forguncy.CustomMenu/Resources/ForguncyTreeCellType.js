var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * ForguncyTreeCellType
 *
 **/
var Forguncy;
(function (Forguncy) {
    var ForguncyTreeCellType = /** @class */ (function (_super) {
        __extends(ForguncyTreeCellType, _super);
        function ForguncyTreeCellType() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.currentTreeValue = null;
            _this.styleSheetInfo = null;
            _this.removeSelectedStyleRules = function () {
                if (this.styleSheetInfo != null) {
                    for (var i = 0; i < this.styleSheetInfo.insertRuleCount; i++) {
                        this.styleSheetInfo.stylesheet.deleteRule(this.styleSheetInfo.stylesheet.cssRules.length - 1);
                    }
                    this.styleSheetInfo = null;
                }
            };
            _this.nodeId = 1;
            _this.tableParamCache = {};
            return _this;
            /************************************/
        }
        ForguncyTreeCellType.prototype.getValueFromElement = function () {
            return this.currentTreeValue;
        };
        //Init tree selected style
        ForguncyTreeCellType.prototype.setValueToElement = function (element, value) {
            //If the tree has default value, the default value won't be saved to currentTreeValue, so once the page reload, the default value will lose.
            if (this.currentTreeValue !== value) {
                this.currentTreeValue = value;
                this.updateTreeActiveState();
            }
            this.selectTreeNode(value);
        };
        ForguncyTreeCellType.prototype.selectTreeNode = function (value) {
            var treeObj = $.fn.zTree.getZTreeObj(this.ID + "_tree");
            var nodes = treeObj.transformToArray(treeObj.getNodes());
            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].tag == value) {
                    treeObj.selectNode(nodes[i]);
                    return;
                }
            }
            var selectedNodes = treeObj.getSelectedNodes();
            if (selectedNodes.length > 0) {
                treeObj.cancelSelectedNode(selectedNodes[0]);
            }
        };
        ForguncyTreeCellType.prototype.createContent = function () {
            var container = $("<div id='" + this.ID + "' class=\"treeContainer\"/>");
            var ul = $("<ul id='" + this.ID + "_tree' class='ztree'></ul>");
            container.append(ul);
            this.addSelectedStyle();
            var self = this;
            this.onDependenceCellValueChanged(function (uiAction) {
                self.initZTree();
                self.selectTreeNode(self.currentTreeValue);
            });
            return container;
        };
        ForguncyTreeCellType.prototype.addSelectedStyle = function () {
            var element = this.CellElement;
            var cellTypeMetaData = element.CellType;
            var treeStyleInfo = cellTypeMetaData.TreeStyleInfo;
            if (treeStyleInfo) {
                for (var i in document.styleSheets) {
                    if (document.styleSheets[i].href && document.styleSheets[i].href.indexOf("forguncyPluginZTreeStyle.css") !== -1) {
                        this.styleSheetInfo = {
                            stylesheet: document.styleSheets[i],
                            insertRuleCount: 0
                        };
                        break;
                    }
                }
                if (this.styleSheetInfo != null) {
                    if (treeStyleInfo.SelectedBackColor && treeStyleInfo.SelectedBackColor != "") {
                        this.styleSheetInfo.stylesheet.insertRule("#" + this.ID + "_tree li a.curSelectedNode { background: " + Forguncy.ConvertToCssColor(treeStyleInfo.SelectedBackColor) + ";}", this.styleSheetInfo.stylesheet.cssRules.length);
                        this.styleSheetInfo.insertRuleCount++;
                    }
                    if (treeStyleInfo.SelectedForeColor && treeStyleInfo.SelectedForeColor != "") {
                        this.styleSheetInfo.stylesheet.insertRule("#" + this.ID + "_tree li a.curSelectedNode { color: " + Forguncy.ConvertToCssColor(treeStyleInfo.SelectedForeColor) + ";}", this.styleSheetInfo.stylesheet.cssRules.length);
                        this.styleSheetInfo.insertRuleCount++;
                    }
                }
            }
        };
        ForguncyTreeCellType.prototype.destroy = function () {
            this.removeSelectedStyleRules();
        };
        ForguncyTreeCellType.prototype.setFontStyle = function (styleInfo) {
            var ul = $("#" + this.ID + "_tree");
            if (styleInfo.FontFamily) {
                ul.css("font-family", styleInfo.FontFamily);
            }
            else {
                ul.css("font-family", Forguncy.LocaleFonts.default);
            }
            if (styleInfo.FontSize && styleInfo.FontSize > 0) {
                ul.css("font-size", styleInfo.FontSize);
                var lineHeight = Math.ceil(styleInfo.FontSize + 8);
                ul.find("li").css("line-height", lineHeight + "px");
                ul.find("li span").css("line-height", lineHeight + "px");
                //Fix bug 26323:If set height for selected node, when click the node, the node will move up.
                //ul.find("li a.curSelectedNode").css("height", lineHeight + "px");
            }
            if (styleInfo.Foreground && styleInfo.Foreground !== "") {
                ul.css("color", Forguncy.ConvertToCssColor(styleInfo.Foreground));
            }
        };
        ForguncyTreeCellType.prototype.getTreeSettings = function () {
            var self = this;
            var setting = {
                view: {
                    showLine: false,
                    dblClickExpand: false
                },
                data: {
                    simpleData: {
                        enable: true
                    }
                },
                callback: {
                    onClick: function (e, treeId, treeNode) {
                        if (treeNode.tag !== self.currentTreeValue) {
                            self.currentTreeValue = treeNode.tag;
                            self.updateTreeActiveState();
                            self.commitValue();
                        }
                        //if value not changed but click the tree item, the command will execute.
                        var commands = self.CellElement.CellType.CommandList;
                        if (commands != null && commands.length > 0) {
                            self.executeCommand(commands);
                        }
                    }
                }
            };
            return setting;
        };
        ForguncyTreeCellType.prototype.getTreeNodes = function () {
            var element = this.CellElement;
            var cellTypeMetaData = element.CellType;
            var bindingTreeLevelInfo = cellTypeMetaData.BindingTreeLevelInfo;
            var isBinding = cellTypeMetaData.IsBinding;
            var treeBindingMode = cellTypeMetaData.TreeBindingMode;
            var items = cellTypeMetaData.Items;
            if (isBinding === true) {
                this.tableParamCache = {};
                this.addTableParamInfo(bindingTreeLevelInfo);
                if (treeBindingMode === 1 /* DynamicLevelMode */) {
                    return this.getTreeNodesInDynamicMode(bindingTreeLevelInfo);
                }
                //转换成Items
                items = this.getBindingItemsInFixLevelMode(bindingTreeLevelInfo, null, "");
            }
            if (items == null || items.length === 0) {
                return [];
            }
            var nodes = this.convertItemsToTreeNodes(items, null, "");
            return nodes;
        };
        /*********************************
        /Add params caches
        */
        ForguncyTreeCellType.prototype.addTableParamInfo = function (bindingTreeLevelInfo) {
            if (!bindingTreeLevelInfo) {
                return;
            }
            var tableName = bindingTreeLevelInfo.TableName;
            if (this.isEmpty(tableName)) {
                return;
            }
            var columns = [];
            this.addTableColumnParamInfo(columns, bindingTreeLevelInfo.NameColumn);
            this.addTableColumnParamInfo(columns, bindingTreeLevelInfo.ValueColumn);
            this.addTableColumnParamInfo(columns, bindingTreeLevelInfo.MasterTableReletedColumn);
            var param = {
                TableName: tableName,
                Columns: columns,
                QueryCondition: bindingTreeLevelInfo.QueryCondition,
                QueryPolicy: {
                    Distinct: false,
                    QueryNullPolicy: Forguncy.QueryNullPolicy.QueryAllItemsWhenValueIsNull,
                    IgnoreCache: false
                },
                SortCondition: bindingTreeLevelInfo.SortCondition
            };
            var postParamWithoutQuery = {
                TableName: tableName,
                Columns: columns,
                QueryPolicy: {
                    Distinct: false,
                    QueryNullPolicy: Forguncy.QueryNullPolicy.QueryAllItemsWhenValueIsNull,
                    IgnoreCache: false
                },
                SortCondition: bindingTreeLevelInfo.SortCondition
            };
            if (!this.tableParamCache[JSON.stringify(bindingTreeLevelInfo)]) {
                this.tableParamCache[JSON.stringify(bindingTreeLevelInfo)] = {};
                this.tableParamCache[JSON.stringify(bindingTreeLevelInfo)].postParam = param;
                this.tableParamCache[JSON.stringify(bindingTreeLevelInfo)].postParamWithoutQuery = postParamWithoutQuery;
            }
            this.addTableParamInfo(bindingTreeLevelInfo.SubBindingTreeLevelInfo);
        };
        ForguncyTreeCellType.prototype.isEmpty = function (obj) {
            if (obj === null || obj === "" || obj === undefined) {
                return true;
            }
            return false;
        };
        ForguncyTreeCellType.prototype.addTableColumnParamInfo = function (list, columnName) {
            if (!this.isEmpty(columnName) && list.indexOf(columnName) === -1) {
                list.push(columnName);
            }
        };
        /*********************************/
        /*********************************
        /get binding items
        */
        ForguncyTreeCellType.prototype.getBindingItemsInFixLevelMode = function (bindingTreeLevelInfo, masterTableRelatedColumnValue, tag) {
            if (!bindingTreeLevelInfo || this.isEmpty(bindingTreeLevelInfo.TableName)) {
                return null;
            }
            var postParam = this.tableParamCache[JSON.stringify(bindingTreeLevelInfo)].postParam;
            var tableData = this.getTableDataByCondition(postParam);
            var items = [];
            if (tableData != null) {
                var flags = {};
                for (var i = 0; i < tableData.length; i++) {
                    //detail table to get master table related column value
                    if (bindingTreeLevelInfo.MasterTableReletedColumn) {
                        var mRCvalue = tableData[i][bindingTreeLevelInfo.MasterTableReletedColumn];
                        if (masterTableRelatedColumnValue != null) {
                            if (mRCvalue !== masterTableRelatedColumnValue) {
                                continue;
                            }
                        }
                    }
                    //去重
                    var value = tableData[i][bindingTreeLevelInfo.ValueColumn];
                    if (value == null || flags[tag + "_" + value] === true) {
                        continue;
                    }
                    flags[tag + "_" + value] = true;
                    var itemInfo = {};
                    var text = tableData[i][bindingTreeLevelInfo.NameColumn];
                    if (text === undefined || text === null) {
                        text = "";
                    }
                    itemInfo.Text = text;
                    itemInfo.Value = value;
                    var detailTableReletedColumnValue = value;
                    if (bindingTreeLevelInfo.SubBindingTreeLevelInfo) {
                        var subItems = this.getBindingItemsInFixLevelMode(bindingTreeLevelInfo.SubBindingTreeLevelInfo, detailTableReletedColumnValue, tag + "_" + value);
                        if (subItems != null) {
                            itemInfo.SubItems = subItems;
                        }
                    }
                    items.push(itemInfo);
                }
            }
            return items;
        };
        // 同步地获取TableData
        ForguncyTreeCellType.prototype.getTableDataByCondition = function (postParam) {
            var tableData = null;
            Forguncy.getTableDataByCondition(postParam, this.getFormulaCalcContext(), function (dataStr) {
                if (dataStr) {
                    tableData = JSON.parse(dataStr);
                }
            }, false);
            return tableData;
        };
        ForguncyTreeCellType.prototype.getTreeNodesInDynamicMode = function (bindingTreeLevelInfo) {
            var paramInfo = this.tableParamCache[JSON.stringify(bindingTreeLevelInfo)];
            var postParam = paramInfo.postParam;
            var postParamWithoutQuery = paramInfo.postParamWithoutQuery;
            var allTableData = this.getTableDataByCondition(postParamWithoutQuery);
            var tableData = this.getTableDataByCondition(postParam);
            if (tableData != null) {
                var flags = [];
                for (var i = 0; i < tableData.length; i++) {
                    var value = tableData[i][bindingTreeLevelInfo.ValueColumn];
                    var pId = tableData[i][bindingTreeLevelInfo.MasterTableReletedColumn];
                    if (!this.isEmpty(pId)) {
                        //if there is a node's parent is itself, we just ignore it.
                        if (value === pId) {
                            continue;
                        }
                        //If there is a node has pId, but the parent node is not showed may be filtered because query;
                        //In this case, the parent node should be show.
                        this.addParentIfNotExistInDynamicMode(bindingTreeLevelInfo, allTableData, pId, flags);
                    }
                    if (flags.indexOf(value) === -1) {
                        flags.push(value);
                    }
                }
                //add nodes
                var nodes = [];
                for (var i = 0; i < allTableData.length; i++) {
                    var value = allTableData[i][bindingTreeLevelInfo.ValueColumn];
                    if (flags.indexOf(value) === -1) {
                        continue;
                    }
                    var text = allTableData[i][bindingTreeLevelInfo.NameColumn];
                    var pId = allTableData[i][bindingTreeLevelInfo.MasterTableReletedColumn];
                    nodes.push({
                        id: value,
                        value: value,
                        name: text,
                        pId: this.isEmpty(pId) ? null : pId,
                        tag: value
                    });
                }
                return nodes;
            }
            return null;
        };
        ForguncyTreeCellType.prototype.addParentIfNotExistInDynamicMode = function (bindingTreeLevelInfo, tableData, pId, flags) {
            for (var i = 0; i < tableData.length; i++) {
                var val = tableData[i][bindingTreeLevelInfo.ValueColumn];
                if (val == pId) {
                    if (flags.indexOf(val) === -1) {
                        flags.push(val);
                    }
                    var cur_pId = tableData[i][bindingTreeLevelInfo.MasterTableReletedColumn];
                    if (!this.isEmpty(cur_pId)) {
                        //if there is a node's parent is itself, we just ignore it.
                        if (cur_pId === pId) {
                            continue;
                        }
                        //If there is a node has pId, but the parent node is not showed may be filtered because query;
                        //In this case, the parent node should be show.
                        this.addParentIfNotExistInDynamicMode(bindingTreeLevelInfo, tableData, cur_pId, flags);
                    }
                    break;
                }
            }
        };
        /*********************************/
        /*********************************
        /convert items to z-tree nodes
        */
        ForguncyTreeCellType.prototype.convertItemsToTreeNodes = function (items, pId, pTag) {
            if (items != null && items.length > 0) {
                var nodes = [];
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    var node = {
                        id: this.nodeId++,
                        value: item.Value,
                        name: item.Text,
                        pId: pId,
                        tag: pTag === "" ? item.Value : (pTag + "_" + item.Value)
                    };
                    nodes.push(node);
                    var subNodes = this.convertItemsToTreeNodes(item.SubItems, node.id, node.tag);
                    if (subNodes != null && subNodes.length > 0) {
                        for (var k = 0; k < subNodes.length; k++) {
                            nodes.push(subNodes[k]);
                        }
                    }
                }
                return nodes;
            }
            return null;
        };
        /*********************************/
        ForguncyTreeCellType.prototype.onLoad = function () {
            $("#" + this.ID).parent("div").css("overflow", "auto");
            this.initZTree();
        };
        ForguncyTreeCellType.prototype.initZTree = function () {
            $.fn.zTree.init($("#" + this.ID + "_tree"), this.getTreeSettings(), this.getTreeNodes());
            var element = this.CellElement;
            var cellTypeMetaData = element.CellType;
            if (cellTypeMetaData.DefaultExpandStyle === 0 /* OpenAll */) {
                var tree = $.fn.zTree.getZTreeObj(this.ID + "_tree");
                tree.expandAll(true);
            }
            this.setFontStyle(element.StyleInfo);
        };
        ForguncyTreeCellType.prototype.getDefaultValue = function () {
            var currentTreeActiveState = this.getTreeActiveState();
            if (currentTreeActiveState) {
                return {
                    Value: currentTreeActiveState
                };
            }
            return null;
        };
        ForguncyTreeCellType.prototype.getTreeActiveState = function () {
            var pageName = this.IsInMasterPage === true ? Forguncy.Page.getMasterPageName() : Forguncy.Page.getPageName();
            if (ForguncyTreeCellType.treeStorage == null || ForguncyTreeCellType.treeStorage[pageName] == null || ForguncyTreeCellType.treeStorage[pageName][this.ID] == null) {
                return;
            }
            var currentTreeStorage = ForguncyTreeCellType.treeStorage[pageName][this.ID];
            return currentTreeStorage["active_treeNode"];
        };
        ForguncyTreeCellType.prototype.updateTreeActiveState = function () {
            if (ForguncyTreeCellType.treeStorage == null) {
                ForguncyTreeCellType.treeStorage = {};
            }
            var pageName = this.IsInMasterPage === true ? Forguncy.ForguncyData.pageInfo.masterPageName : Forguncy.ForguncyData.pageInfo.pageName;
            if (ForguncyTreeCellType.treeStorage[pageName] == null) {
                ForguncyTreeCellType.treeStorage[pageName] = {};
            }
            if (ForguncyTreeCellType.treeStorage[pageName][this.ID] == null) {
                ForguncyTreeCellType.treeStorage[pageName][this.ID] = [];
            }
            ForguncyTreeCellType.treeStorage[pageName][this.ID]["active_treeNode"] = this.currentTreeValue;
        };
        /***********************************
        /Reload tree when table data changed.
        */
        ForguncyTreeCellType.prototype.getBindingTables = function () {
            var cellTypeMetaData = this.CellElement.CellType;
            var isBinding = cellTypeMetaData.IsBinding;
            var bindingTreeLevelInfo = cellTypeMetaData.BindingTreeLevelInfo;
            if (!isBinding || !bindingTreeLevelInfo) {
                return null;
            }
            var result = [];
            var treeBindingMode = cellTypeMetaData.TreeBindingMode;
            if (treeBindingMode == 1 /* DynamicLevelMode */) {
                result.push(bindingTreeLevelInfo.TableName);
            }
            else {
                this.addRelatedTables(bindingTreeLevelInfo, result);
            }
            return result;
        };
        ForguncyTreeCellType.prototype.addRelatedTables = function (bindingTreeLevelInfo, list) {
            if (bindingTreeLevelInfo) {
                list.push(bindingTreeLevelInfo.TableName);
                this.addRelatedTables(bindingTreeLevelInfo.SubBindingTreeLevelInfo, list);
            }
        };
        //close popup may not need reload.
        ForguncyTreeCellType.prototype.reload = function () {
            var isReload = arguments[0];
            if (isReload) {
                this.initZTree();
                this.selectTreeNode(this.currentTreeValue);
            }
        };
        ForguncyTreeCellType.treeStorage = {};
        return ForguncyTreeCellType;
    }(Forguncy.Plugin.CellTypeBase));
    Forguncy.ForguncyTreeCellType = ForguncyTreeCellType;
})(Forguncy || (Forguncy = {}));
Forguncy.Plugin.CellTypeHelper.registerCellType("Forguncy.CustomMenu.ForguncyTreeCellType, Forguncy.CustomMenu", Forguncy.ForguncyTreeCellType);
//# sourceMappingURL=ForguncyTreeCellType.js.map
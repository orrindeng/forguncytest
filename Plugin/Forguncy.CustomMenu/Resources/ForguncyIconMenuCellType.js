var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * ForguncyIconMenuCellType
 *
 **/
var Forguncy;
(function (Forguncy) {
    var ForguncyIconMenuCellType = /** @class */ (function (_super) {
        __extends(ForguncyIconMenuCellType, _super);
        function ForguncyIconMenuCellType() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.defaultItemATag = null;
            _this.menuContainerName = "iconMenuContainer";
            return _this;
        }
        ForguncyIconMenuCellType.prototype.createContent = function () {
            var container = Forguncy.MenuCellTypeBase.prototype.createContent.call(this);
            container.css("z-index", "9999");
            return container;
        };
        ForguncyIconMenuCellType.prototype.initMenuItemStyle = function (cellTypeMetaData, container) {
            this.setFirstItemLevelStyle(cellTypeMetaData, container);
        };
        ForguncyIconMenuCellType.prototype.initMenuItems = function (itemsInfo, levelsStyle, levelIndex, aTag) {
            if (itemsInfo != null && itemsInfo.length > 0) {
                var itemsLength = itemsInfo.length;
                var ul = $("<ul level=" + levelIndex + "></ul>");
                for (var i = 0; i < itemsLength; i++) {
                    var itemInfo = itemsInfo[i];
                    //check authority
                    if (this.checkAuthority(itemInfo) === false) {
                        continue;
                    }
                    var li = $("<li></li>");
                    //append hyperlink
                    var a = this.createHyperlinkHtml(itemInfo, aTag + ";index=" + i + "");
                    li.append(a);
                    //append li
                    ul.append(li);
                }
                //set menu level style
                this.setMenuLevelStyle(ul, levelsStyle[levelIndex], levelIndex);
                return ul;
            }
            return null;
        };
        ForguncyIconMenuCellType.prototype.createHyperlinkHtml = function (itemInfo, aTag) {
            var a = $("<a aTag='" + aTag + "'></a>");
            itemInfo.aTag = aTag;
            if (itemInfo.IsDefaultItem) {
                this.defaultItemATag = aTag;
            }
            // Fix: icon on mobile's can't show wholely (BugX #6915)
            var iconWidth = 30;
            var iconHeight = 30;
            var iconRowHeight = 36;
            var icon, selectedIcon;
            if (itemInfo.IconPath && itemInfo.IconPath != "") {
                icon = this.createIconHtml(itemInfo.IconPath, itemInfo.IconColor, iconRowHeight, iconWidth, iconHeight, itemInfo.IsBuiltInIconPath, itemInfo.IsOldMenuPath);
            }
            else {
                icon = this.createIconHtml("", "", iconRowHeight, iconWidth, iconHeight, false, false);
            }
            if (itemInfo.IconPath && itemInfo.IconPath != "") {
                selectedIcon = this.createIconHtml(itemInfo.SelectedIconPath, itemInfo.SelectedIconColor, iconRowHeight, iconWidth, iconHeight, itemInfo.IsBuiltInSelectedIconPath, itemInfo.IsOldMenuPath);
            }
            else {
                selectedIcon = this.createIconHtml("", "", iconRowHeight, iconWidth, iconHeight, false, false);
            }
            icon.attr("IconType", "Icon");
            selectedIcon.attr("IconType", "SelectedIcon");
            selectedIcon.hide();
            a.append(icon);
            a.append(selectedIcon);
            //append text
            var text = itemInfo.Text;
            if (text === undefined || text === null) {
                text = "";
            }
            a.append($("<span class='menuText'>" + text + "</span>"));
            if (itemInfo.Notification) {
                var span = $("<span></span>");
                span.css("background-color", "red");
                span.css("color", "white");
                span.css("font-size", 12);
                span.css("font-weight", 700);
                span.css("text-align", "center");
                span.css("font-family", Forguncy.LocaleFonts.default);
                span.css("font-style", "normal");
                span.css("display", "none");
                span.css("position", "absolute");
                span.css("top", 0);
                span.css("left", "55%");
                span.css("height", "15px");
                span.css("width", "15px");
                span.css("border-radius", "10px");
                span.css("text-overflow", "ellipsis");
                span.css("line-height", "15px");
                span.css("overflow", "hidden");
                span.css("padding", "2px");
                this.notifyNodes.push({ "dom": span, "formula": itemInfo.Notification });
                a.append(span);
            }
            //add css style
            a.css("display", "block");
            a.css("cursor", "pointer");
            a.css("text-decoration", "none");
            a.css("padding", "2px 0 5px 0");
            a.css("position", "relative");
            var self = this;
            a.click(function () {
                self.setSelectItem($(this).attr("aTag"));
                self.executeCommand(itemInfo.CommandList);
                return false;
            });
            return a;
        };
        ForguncyIconMenuCellType.prototype.createIconHtml = function (iconPath, iconColor, itemHeight, iconWidth, iconHeight, isBuiltIn, isOldPath) {
            var imgHtml = _super.prototype.createIconHtml.call(this, iconPath, iconColor, itemHeight, iconWidth, iconHeight, isBuiltIn, isOldPath);
            // 老版本的PC菜单和手机图标菜单存储图片的时候位置不一样，这里为了保持兼容性，强行修改获取菜单图标的路径
            if (isOldPath && !isBuiltIn) {
                var bgImage = imgHtml.css("background-image");
                if (bgImage) {
                    bgImage = bgImage.replace(Forguncy.Helper.SpecialPath.getUploadFileFolderPathInDesigner() + "ForguncyCustomMenu/IconImages/", Forguncy.Helper.SpecialPath.getImageEditorUploadImageFolderPath());
                    imgHtml.css("background-image", bgImage);
                }
            }
            // ----------------------------------------
            imgHtml.css("margin-top", (itemHeight - iconHeight) / 2 + "px");
            return imgHtml;
        };
        ForguncyIconMenuCellType.prototype.onLoad = function () {
            var parentDIV = $("#" + this.ID).parent("div");
            parentDIV.width(0);
            parentDIV.css("overflow", "");
            this.initNotifyNumber();
            var pageName = this.IsInMasterPage === true ? Forguncy.Page.getMasterPageName() : Forguncy.Page.getPageName();
            if (Forguncy.MenuCellTypeBase.menuStorage == null || Forguncy.MenuCellTypeBase.menuStorage[pageName] == null || Forguncy.MenuCellTypeBase.menuStorage[pageName][this.ID] == null) {
                if (this.defaultItemATag) {
                    this.setSelectedItemStyle(this.defaultItemATag);
                }
                return;
            }
            var currentMenuStorage = Forguncy.MenuCellTypeBase.menuStorage[pageName][this.ID];
            for (var levelIndex = 0; levelIndex < currentMenuStorage.length; levelIndex++) {
                if (currentMenuStorage[levelIndex] == null) {
                    return;
                }
                for (var tag in currentMenuStorage[levelIndex]) {
                    var isExpand = currentMenuStorage[levelIndex][tag];
                    this.setMenuStateForElement(levelIndex, tag, isExpand);
                }
            }
            if (currentMenuStorage["active_a_tag"]) {
                this.setSelectItem(currentMenuStorage["active_a_tag"]);
            }
        };
        ForguncyIconMenuCellType.prototype.setSelectItem = function (aTag) {
            this.setSelectedItemStyle(aTag);
            _super.prototype.setSelectItem.call(this, aTag);
        };
        ForguncyIconMenuCellType.prototype.setSelectedItemStyle = function (aTag) {
            $(".iconMenuContainer ul li a").removeClass("active" /* Active */);
            $("#" + this.ID).find("a[aTag='" + aTag + "']").addClass("active" /* Active */);
            var selectedIndex = $("#" + this.ID + " ul li a").index($("#" + this.ID).find("a[aTag='" + aTag + "']"));
            var metaData = this.CellElement.CellType;
            for (var i = 0; i < metaData.Items.length; i++) {
                var item = metaData.Items[i];
                var icon = $("#" + this.ID).find("a[aTag='" + item.aTag + "'] [IconType='Icon']");
                var selectedIcon = $("#" + this.ID).find("a[aTag='" + item.aTag + "'] [IconType='SelectedIcon']");
                if (selectedIndex == i) {
                    selectedIcon.show();
                    icon.hide();
                }
                else {
                    selectedIcon.hide();
                    icon.show();
                }
            }
        };
        ForguncyIconMenuCellType.prototype.setFontStyle = function (styleInfo) {
            var a = $("#" + this.ID + " ul li a");
            if (styleInfo.FontFamily && styleInfo.FontFamily !== "") {
                a.css("font-family", styleInfo.FontFamily);
            }
            if (styleInfo.FontSize && styleInfo.FontSize > 0) {
                a.css("font-size", styleInfo.FontSize);
            }
            if (styleInfo.FontStyle) {
                a.css("font-style", styleInfo.FontStyle.toLowerCase());
            }
            if (styleInfo.FontWeight) {
                a.css("font-weight", styleInfo.FontWeight.toLowerCase());
            }
        };
        return ForguncyIconMenuCellType;
    }(Forguncy.MenuCellTypeBase));
    Forguncy.ForguncyIconMenuCellType = ForguncyIconMenuCellType;
})(Forguncy || (Forguncy = {}));
Forguncy.Plugin.CellTypeHelper.registerCellType("Forguncy.CustomMenu.ForguncyIconMenuCellType, Forguncy.CustomMenu", Forguncy.ForguncyIconMenuCellType);
//# sourceMappingURL=ForguncyIconMenuCellType.js.map